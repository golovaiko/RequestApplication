namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changetypeforstatusinrequest : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Requests", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Requests", "Status", c => c.String());
        }
    }
}
