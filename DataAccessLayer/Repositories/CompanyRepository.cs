﻿using BussinessLogicLayer.Models;
using BussinessLogicLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class CompanyRepository : Repository<Company , string>, ICompanyRepository
    {
        public CompanyRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public IEnumerable<Company> GetCompaniesByOfferId(int? id)
        {
            var companyIds = UnitOfWork.Context.CompanyOffers
                .Where(compOffer => id.HasValue && compOffer.OfferId == id)
                .Select(compOffer => compOffer.CompanyId)
                .ToList();

            return UnitOfWork.Context.Companies.Where(comp => companyIds.Any(companiesId => comp.Id == companiesId)).ToList();
        }

        public IEnumerable<Company> GetCompaniesFromOrderHistories(List<string> OrderHistoriesUserIds)
        {
            return UnitOfWork.Context.Companies
                .Where(x => OrderHistoriesUserIds.Contains(x.Id))
                .ToList();
        }
    }
}
