﻿using BussinessLogicLayer.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Services
{
    public interface IRequestCreationService
    {
        void CreateNewRequest(NewRequestDTO rqDTO);
    }
}
