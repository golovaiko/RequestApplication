﻿var token = sessionStorage.getItem('tokenKey');

$('#manageOffersButton').click(function () {
    $('#manageOffersModalBody').trigger('reset');
    CategoryProvidedOfferAjax('/API/RequestAPI/GetCategories');
})

CategoryProvidedOfferAjax('/API/RequestAPI/GetCategories');

$('#categoryCompanyDropDownList').change(function () {
    ProvidedOfferAjax('/API/RequestAPI/GetProvidedOffers?categoryId=', $('#categoryCompanyDropDownList').val())
});

$('#saveManagedOffersButton').click(function () {
    SaveCompanyProvidedOffers("/API/RequestAPI/SaveProvidedOffers");
})


function CreateTableHeader() {
    $("#offerTableForCompany").append($('<tr />', { id: "offerTableHeadRow" }));
    $("#offerTableHeadRow").append($('<th/>', { colspan: "1", id: 'offerTableHeadBox1' }));
    $("#offerTableHeadBox1").append($("<p />", { text: "Offers list" }));
    $("#offerTableHeadRow").append($('<th/>', { id: 'offerTableHeadBox2' }));
    $("#offerTableHeadBox2").append($("<p />", { text: "Is providing" }));
    $("#offerTableHeadRow").append($('<th/>', { id: 'offerTableHeadBox3' }));
    $("#offerTableHeadBox3").append($("<p />", { text: "Offer Price" }));
}

var currentOffersCollection;

function CategoryProvidedOfferAjax(url) {
    $.ajax({
        url: url,
        type: 'GET',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (categories) {
            $('#categoryCompanyDropDownList').html('');
            var categoryOptions = '';
            for (var i = 0; i < categories.length; i++) {
                categoryOptions += '<option value=' + categories[i].Id + '>' + categories[i].Name + '</option>';
            }
            $('#categoryCompanyDropDownList ').append(categoryOptions);
            $.ajax({
                url: '/API/RequestAPI/GetProvidedOffers?categoryId=' + Number($('#categoryCompanyDropDownList').val()),
                type: 'GET',
                headers: {
                    "Accept": "application/json",
                    "Authorization": "Bearer " + token
                },
                success: function (offersCollection) {
                    currentOffersCollection = offersCollection;
                    $("#offerTableForCompany").empty();
                    CreateTableHeader();
                    if (offersCollection.OfferDTOs.length > 0) {
                        $.each(offersCollection.OfferDTOs, function (i, offer) {
                            $("#offerTableForCompany").append($('<tr />', { id: "offerTableRow" + i }));
                            $("#offerTableRow" + i).append($('<td/>', { id: 'offerTableBox' + (i + 1) + i }));
                            $('#offerTableBox' + (i + 1) + i).append($('<label />', { text: offer.Offer.Name, style: "width:auto" }));
                            $("#offerTableRow" + i).append($('<td/>', { id: 'offerTableBox' + (i + 1) + (i + 1) }));
                            if (offersCollection.OfferDTOs[i].IsProviding) {
                                $('#offerTableBox' + (i + 1) + (i + 1)).append($('<input />', { type: 'checkbox', checked: "checked", id: "providedCheckBox" + i }));

                            }
                            else {
                                $('#offerTableBox' + (i + 1) + (i + 1)).append($('<input />', { type: 'checkbox', id: "providedCheckBox" + i }));
                            }
                            $("#offerTableRow" + i).append($('<td/>', { id: 'offerTableBox' + (i + 1) + (i + 2) }));
                            $('#offerTableBox' + (i + 1) + (i + 2)).append($('<input />', { id: "offerPriceTextBox" + i, type: 'text', value: offer.OfferPrice }));

                        });
                    }
                },
                error: function (error) { console.log(error) }
            });
        },
        error: function (error) { console.log(error) }
    });
}

function ProvidedOfferAjax(url, neededId) {
    $.ajax({
        url: url + Number(neededId),
        type: 'GET',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (offersCollection) {
            currentOffersCollection = offersCollection;
            $("#offerTableForCompany").empty();
            CreateTableHeader();
            if (offersCollection.OfferDTOs.length > 0) {
                $.each(offersCollection.OfferDTOs, function (i, offer) {
                    $("#offerTableForCompany").append($('<tr />', { id: "offerTableRow" + i }));
                    $("#offerTableRow" + i).append($('<td/>', { id: 'offerTableBox' + (i + 1) + i }));
                    $('#offerTableBox' + (i + 1) + i).append($('<label />', { text: offer.Offer.Name, style: "width:auto" }));
                    $("#offerTableRow" + i).append($('<td/>', { id: 'offerTableBox' + (i + 1) + (i + 1) }));
                    if (offersCollection.OfferDTOs[i].IsProviding) {
                        $('#offerTableBox' + (i + 1) + (i + 1)).append($('<input />', { type: 'checkbox', checked: "checked", id: "providedCheckBox" + i }));

                    }
                    else {
                        $('#offerTableBox' + (i + 1) + (i + 1)).append($('<input />', { type: 'checkbox', id: "providedCheckBox" + i }));
                    }
                    $("#offerTableRow" + i).append($('<td/>', { id: 'offerTableBox' + (i + 1) + (i + 2) }));
                    $('#offerTableBox' + (i + 1) + (i + 2)).append($('<input />', { id: "offerPriceTextBox" + i, type: 'text', value: offer.OfferPrice }));
                });
            }
        },
        error: function (error) { console.log(error) }
    });
}

function SaveCompanyProvidedOffers(url) {
    var providedOffers = currentOffersCollection;
    $.each(providedOffers.OfferDTOs, function (i, offer) {
        if ($("#providedCheckBox" + i).prop("checked")) {
            offer.IsProviding = true;
            offer.OfferPrice = $("#offerPriceTextBox" + i).val();
        }
        else
            offer.IsProviding = false
    }
    );
    $.ajax({
        url: url,
        type: 'POST',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        data: providedOffers,
        dataType: "json",
        success: function () {
            $.ambiance({
                message: "Saved successfull!",
                title: "Success!",
                type: "success"
            })
            $("#closeManageOffersModal").click();
        },
        error: function () {
            $.ambiance({
                message: "Error try again please!",
                title: "Error!",
                type: "error"
            })
            $("#closeManageOffersModal").click();
        }
    });
}