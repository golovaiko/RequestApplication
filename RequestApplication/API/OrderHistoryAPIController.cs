﻿using AutoMapper;
using BussinessLogicLayer.Models;
using BussinessLogicLayer.Models.DTO;
using BussinessLogicLayer.Repositories;
using BussinessLogicLayer.Services;
using BussinessLogicLayer.Services.Statuses;
using Ninject;
using RequestApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace RequestApplication.API
{
    [Authorize] 
    public class OrderHistoryAPIController : BaseApiController
    {
        private IOrderHistoryService _orderHistoryService;
        private IOrderHistoryRepository _orderHistoryRepository;
        private IOrderRepository _orderRepository;
        private IRequestRepository _requestRepository;
        [Inject]
        public OrderHistoryAPIController(IOrderHistoryService orderHistoryService, IOrderHistoryRepository orderHistoryRepository, IOrderRepository orderRepository, IRequestRepository requestRepository)
        {
            _orderHistoryService = orderHistoryService;
            _orderHistoryRepository = orderHistoryRepository;
            _orderRepository = orderRepository;
            _requestRepository = requestRepository;
        }
        public OrderHistoryAPIController()
        {
        }

        [HttpGet]
        public IEnumerable<OrderHistoryDTO> GetAll(int id)
        {
            List<OrderHistory> listOrderHistory = _orderHistoryService.Find(id).Reverse().ToList();
            List<OrderHistoryDTO> listOrderHistoryVM = new List<OrderHistoryDTO>();
            foreach (var x in listOrderHistory)
            {
                listOrderHistoryVM.Add(OrderHistoryDTO.Init(x, _orderHistoryService.AdditionInfoAboutUser(x.UserId)));
            }
            return listOrderHistoryVM;
        }
        [HttpGet]
        public Request GetRequest(int id)
        {
            return _orderHistoryService.GetRequest(id);
        }
        [HttpPost]
        public void AddInOrderHistory(OrderHistoryDTO orderHistoryDTO, int currentStatus)
        {
            int requestCurrentStatus= _orderHistoryService.GetCurrentRequestForOrderHistory(orderHistoryDTO).Status;
            if (ModelState.IsValid && (requestCurrentStatus != (int)StatusesForRequest.Canceled && requestCurrentStatus != (int)StatusesForRequest.Archived))
            {
                orderHistoryDTO.UserId = UserId;
                    _orderHistoryService.AddOnlyWithAC(orderHistoryDTO, currentStatus);
            }
        }
        [HttpPost]
        public void AddInOrderHistoryCooperation(int orderId)
        {
            OrderHistoryDTO model = new OrderHistoryDTO
            {
                Order = _orderRepository.GetById(orderId),
                Status = (int)StatusesForOrderHistory.Pending,
                UpdateDate = DateTime.Now,
            };
            AddInOrderHistory(model, 1);
        }

        [HttpGet]
        public int СheckIfThereIsStatusAcceptedOrArchived(int idRequest)
        {
            return _orderHistoryService.CheckIfAgreed(idRequest);
        }


        [HttpPost]
        public void ChangeRequest(ChangeRequestDataViewModel model)
        {

            if (ModelState.IsValid == true)
            {
                var orderHistory = _orderHistoryService.GetOrderHistoryById(model.Id);
                if (model.ScheduleDate != orderHistory.ScheduleDate || Convert.ToString(model.Price) != orderHistory.Price)
                {
                    Mapper.Initialize(cfg => cfg.CreateMap<ChangeRequestDataViewModel, OrderHistoryDTO>()
                    .ForMember("UpdateDate", opt => opt.MapFrom(c => DateTime.Now))
                    .ForMember("Order", opt => opt.MapFrom(c => orderHistory.Order))
                    .ForMember("UserId", opt => opt.MapFrom(c => UserId)));
                    OrderHistoryDTO NewOrderHistory = Mapper.Map<ChangeRequestDataViewModel, OrderHistoryDTO>(model);
                    Mapper.Reset();
                    AddInOrderHistory(NewOrderHistory, (int)StatusesForOrderHistory.Changed);
                }
            }

        }
    }
}