﻿using BussinessLogicLayer.Core;
using BussinessLogicLayer.Repositories;
using BussinessLogicLayer.Services;
using DataAccessLayer;
using DataAccessLayer.Repositories;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;
using RequestApplication;
using System;
using System.Reflection;
using System.Web;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace RequestApplication
{
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }


        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
           // GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
            RegisterServices(kernel);
            return kernel;
        }
        private static void RegisterServices(IKernel kernel)
        {
            //kernel.Bind<IOrderHistoryRepository>().ToMethod(ctx => new OrderHistoryRepository());
            kernel.Bind<ICategoryRepository>().To<CategoryRepository>();
            kernel.Bind<ICompanyRepository>().To<CompanyRepository>();
            kernel.Bind<ICustomerRepository>().To<CustomerRepository>();
            kernel.Bind<IOrderRepository>().To<OrderRepository>();
            kernel.Bind<IOfferRepository>().To<OfferRepository>();
            kernel.Bind<ICompanyOfferRepository>().To<CompanyOfferRepository>();
            kernel.Bind<IOrderHistoryRepository>().To<OrderHistoryRepository>();
            kernel.Bind<IRequestRepository>().To<RequestRepository>();
            //services
            kernel.Bind<IOrderHistoryService>().To<OrderHistoryService>();
            kernel.Bind<INotificationsService>().To<NotificationsService>();
            kernel.Bind<IOrderCreationService>().To<OrderCreationService>();
            kernel.Bind<IRequestStatusCountService>().To<RequestStatusCountService>();
            kernel.Bind<IRequestCreationService>().To<RequestCreationService>();
            kernel.Bind<IManageOffersService>().To<ManageOffersService>();
            kernel.Bind<IUserDataUpdateService>().To<UserDataUpdateService>();
            //kernel.Bind<IUnitOfWork>().To<UnitOfWork>();

        }
    }
}