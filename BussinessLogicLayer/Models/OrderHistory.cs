﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Models
{
    public class OrderHistory
    {
        public int Id { get; set; }
        [Required]
        public int Status { get; set; }
        [Required]
        public string Changed { get; set; }
        [Required]
        public string Price { get; set; }
        [Required]
        public DateTime ScheduleDate { get; set; }
        [Required]
        public DateTime UpdateDate { get; set; }
        public Order Order { get; set; }
        public bool IsShown { get; set; } 
        public string UserId { get; set; }
    }
}
