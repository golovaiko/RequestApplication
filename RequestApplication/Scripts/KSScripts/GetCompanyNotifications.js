﻿
$(document).ready(function () {
    GetCompanyNotifications();
});

function GetCompanyNotifications() {
    $("#listNotifications").empty();
    $.ajax({
        url: '/api/NotificationsAPI/GetCompanyNotifications',
        type: 'GET',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        dataType: 'json',
        success: function (data) {
            $(data).each(function (key, instance) {
                var hiddenId = $('<input>');
                hiddenId.attr('type', 'hidden');
                hiddenId.attr('id', 'hiddenId');
                hiddenId.attr('value', instance.Id);
                var hiddenOrderId = $('<input>');
                hiddenOrderId.attr('type', 'hidden');
                hiddenOrderId.attr('id', 'hiddenOrderId');
                hiddenOrderId.attr('value', instance.OrderId);
                var hiddenRequestId = $('<input>');
                hiddenRequestId.attr('type', 'hidden');
                hiddenRequestId.attr('id', 'hiddenRequestId');
                hiddenRequestId.attr('value', instance.RequestId);

                var isActive = instance.IsShown;
                var listItem = $('<div>');

                var instanceRecord = $('<a>');
               
                var badgeSection = $('<h5><span class="badge badge-danger">New</span></h5>');
                instanceRecord.addClass('list-group-item flex-column align-items-start companyNotification');
                instanceRecord.addClass('companyNotification');
                instanceRecord.attr('style','padding-top:10px;padding-right:10px;padding-bottom:0px;padding-left:10px;');
                if (isActive == false) {
                    instanceRecord.addClass("newNotification");
                    instanceRecord.addClass('list-group-item-action');
                    instanceRecord.append(badgeSection);
                    listItem.addClass('falseAction');
                }
                else {
                    listItem.addClass('trueAction');
                    instanceRecord.addClass("trueNotification");
                }
                addOfferCategory = $('<div>');
                addOfferCategory.addClass('d-flex w-100 justify-content-between');
                addOfferCategory.append("<h7><i>" + instance.OfferCategory + "</i></h7>");

                var addDivSection = $('<div>');
                addDivSection.addClass('d-flex w-100 justify-content-between');
                var h5Section = $('<h5>');
                h5Section.addClass('mb-1 font-weight-bold');
                h5Section.text(" Customer " + "'" + instance.Name + "'");
                var smallSection = $('<small>');
                smallSection.addClass('text-muted');
                var date = new Date(instance.CreateDate);
                smallSection.text(date.toLocaleString());
                var isection = $('<i>');
                isection.addClass('fas fa-user');
                isection.attr('id','userSign');

                h5Section.prepend(isection);
                addDivSection.append(h5Section);
                addDivSection.append(smallSection);
                instanceRecord.append(addDivSection);

                var newDiv = $('<div>');
                var newH5 = $('<h6>');
                var isec = $('<b>');
                newH5.append(isec);
                newH5.text(instance.Message);
                newDiv.append(newH5);
                instanceRecord.append(addOfferCategory);
                instanceRecord.append(newDiv);

                var pSection = $('<p>');
                pSection.addClass('mb-1');
                var date2 = new Date(instance.ScheduleDate);
                pSection.text("  " + date2.toLocaleString());

                var calendarImg = $('<i>');
                calendarImg.addClass('fas fa-calendar-alt');
                calendarImg.attr('id', 'calendarSign');
                pSection.append(calendarImg);

                var addDivSection2 = $('<div>');
                addDivSection2.addClass('d-flex w-100 justify-content-between');
                pSection.prepend(calendarImg);
                addDivSection2.append(pSection);

                var priceSection = $('<p>');
                var priceImg = $('<i>');
                priceImg.attr('id','dollarSign');
                priceImg.addClass("fas fa-dollar-sign");
                priceSection.text("  " + instance.Price);
                priceSection.prepend(priceImg);
                addDivSection2.append(priceSection);
                instanceRecord.append(addDivSection2);
                listItem.append(instanceRecord);
                listItem.append(hiddenId);
                listItem.append(hiddenOrderId);
                listItem.append(hiddenRequestId);
                $(".list-group").append(listItem);
            });


        },
        error: function () {
            //alert('Error!');
        }
    });
};


