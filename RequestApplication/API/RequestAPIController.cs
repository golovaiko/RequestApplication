﻿using BussinessLogicLayer.Models;
using BussinessLogicLayer.Models.DTO;
using BussinessLogicLayer.Repositories;
using BussinessLogicLayer.Services;
using RequestApplication.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace RequestApplication.API
{
    [Authorize]
    public class RequestAPIController : BaseApiController
    {
        readonly IRequestRepository _requestRepository;
        readonly IOrderRepository _orderRepository;
        readonly ICompanyRepository _companyRepository;
        readonly IOfferRepository _offerRepository;
        readonly ICategoryRepository _categoryRepository;
        readonly ICompanyOfferRepository _companyOfferRepository;
        readonly IOrderCreationService _orderCreationService;
        readonly IRequestStatusCountService _requestStatusCountService;
        readonly IRequestCreationService _requestCreationService;
        readonly IManageOffersService _manageOffersService;

        public RequestAPIController(IRequestRepository requestRepository, 
            IOrderRepository orderRepository, 
            ICompanyRepository companyRepository,
            IOfferRepository offerRepository, 
            ICategoryRepository categoryRepository, 
            ICompanyOfferRepository companyOfferRepository,
            IOrderCreationService orderCreationService,
            IRequestStatusCountService requestStatusCountService,
            IRequestCreationService requestCreationService,
            IManageOffersService manageOffersService)
        {
            _requestStatusCountService = requestStatusCountService;
            _requestRepository = requestRepository;
            _orderRepository = orderRepository;
            _companyRepository = companyRepository;
            _offerRepository = offerRepository;
            _categoryRepository = categoryRepository;
            _companyOfferRepository = companyOfferRepository;
            _orderCreationService = orderCreationService;
            _requestCreationService = requestCreationService;
            _manageOffersService = manageOffersService;
        }


        [HttpGet]
        [Authorize]
        public List<Category> GetCategories()
        {

            return _categoryRepository.GetAll().ToList();
        }

        [HttpGet]
        [Authorize]
        public List<Offer> GetOffers(int categoryId)
        {
            return _offerRepository.GetAll().Where(i => i.CategoryId == categoryId).ToList();
        }

        [HttpGet]
        [Authorize]
        public OfferCollectionDTO GetProvidedOffers(int categoryId)
        {
            return _manageOffersService.GetProvidedOffers(UserId,categoryId);
        }

        [Authorize]
        public void SaveProvidedOffers(OfferCollectionDTO model)
        {
            model.CompanyId = UserId;
            if (ModelState.IsValid)
            {
                _manageOffersService.SaveProvidedOffers(model);
            }
        }

        [Authorize]
        public void CreateNewRequest(NewRequestDTO rq)
        {
            rq.CustomerId = UserId;
            if (ModelState.IsValid)
            {
                _requestCreationService.CreateNewRequest(rq);
                _orderCreationService.CreateOrdersByRequest(rq.Deadline);
            }
        }

        [HttpGet]
        [Authorize]
        public List<Request> GetRequests([FromUri]int status)
        {
            return _requestRepository.GetRequestByIdAndStatus(UserId,status);
        }

        [HttpGet]
        [Authorize]
        public List<Order> GetOrders([FromUri]int requestId)
        {
            return _orderRepository.GetByRequestId(requestId);
        }

        [HttpGet]
        [Authorize]
        public List<Request> GetCompanyOrders([FromUri]string requestId)
        {

            return _orderRepository.GetByCompanyId(requestId);
        }


        [HttpGet]
        [Authorize]
        public List<OrderViewModel> PostOrders(string id)
        {
            List<Order> listOrders = _orderRepository.GetByRequestId(int.Parse(id));
            List<OrderViewModel> orderViewModel = new List<OrderViewModel>();
            foreach (var order in listOrders)
            {
                var company = _companyRepository.GetById(order.CompanyId);
                OrderViewModel owm = OrderViewModel.Init(order, company);
                orderViewModel.Add(owm);
            }

            return orderViewModel;
        }

        [HttpPost]
        [Authorize]
        public void ChangeStatus(int status, int requestId)
        {
            var request = _requestRepository.GetById(requestId);
            request.Status = status;
            _requestRepository.Update(request);
            _requestRepository.SaveChanges();
        }

        [HttpGet]
        public Dictionary<string,int> GetRequestsStatusCount()
        {
            return _requestStatusCountService.GetRequestsStatusCount(UserId);
        }
    }
}
