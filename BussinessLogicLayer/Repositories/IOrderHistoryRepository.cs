﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessLogicLayer.Core;
using BussinessLogicLayer.Models;

namespace BussinessLogicLayer.Repositories
{
    public interface IOrderHistoryRepository : IRepository<OrderHistory, int>
    {
        IEnumerable<OrderHistory> FindWithOrder(int orderId);
        IEnumerable<OrderHistory> GelAllWithOrder(Func<OrderHistory, bool> predicate);
        IEnumerable<OrderHistory> GelAllWithOrder();
        List<OrderHistory> GetOrderHistories(List<int> requestIds);
        OrderHistory GetOrderHistoryWithOrder(int orderHistoryId);
        OrderHistory GetLastWithOrder(int OrderId);
        List<OrderHistory> GetCompanyOrderHistories(string CurrentUserId);
        void ChangeOrderHistoryActiveStatusByOrderId(int orderId);


    }
}
