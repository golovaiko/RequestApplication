﻿using BussinessLogicLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RequestApplication.Models
{
    public class OrderViewModel
    {
        public int OrderId { get; set; }
        public int RequestId { get; set; }
        public string CompanyId { get; set; }
        public string Price { get; set; }
        public DateTime ExecutionDate { get; set; }
        public string companyName { get; set; }
        public string ProfileImage { get; set; }

        public static OrderViewModel Init(Order order, Company company)
        {
            OrderViewModel ovm = new OrderViewModel() { OrderId = order.Id, RequestId = order.RequestId, CompanyId = order.CompanyId, Price = order.Price, ExecutionDate = order.ExecutionDate, companyName = company.Name };
            ovm.ProfileImage = "data:image/jpg;base64," + Convert.ToBase64String(company.ProfileImage, 0, company.ProfileImage.Length);
            return ovm;
        }
    }
}

