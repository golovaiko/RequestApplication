﻿
var token = sessionStorage.getItem('tokenKey');
function createMainDomElements() {
    return {
        mainContainer:
            {
                className: 'col requests-container',
                id: 'tabsOrderHistory',
                orderHistoryList: {
                    className: 'items',
                    id: 'orderList',
                    blockMessages: {
                        className: "timeline-centered",
                    }
                }

            }
    }
}

//variables that are needed for other functions and dynamically change
chek = 0;
var orderId;
var requestId;

function getOrderHistories(id) {
    var btnMessages = $(id);
    orderId = btnMessages.data("order-id");
    console.log("orderId: ", orderId);
    requestId = btnMessages.data("request-id");
    if (chek != 0 && chek != orderId) {
        $("div#tabsOrderHistory").remove();
        $('.item').css('background-color', 'white');
    }
    var mainBlock = CreateMainBlock();
    ShowMessages(orderId, requestId);
    if (chek == orderId) {
        $("div#tabsOrderHistory").remove();
        $('.item').css('background-color', 'white');
        chek = 0;
    }
    else {
        chek = orderId;
        $('.item').css('background-color', '');
        $('.item.' + orderId).css('background-color', '#F3F3F3');
    }

}

setInterval(function () {
    if ($("#orderList").hasClass("items")) {
        if (!$("#multiCollapseExample").hasClass("show"))
            ShowMessages(orderId, requestId);
    }
}, 8000);

function createDomElements() {
    return {
        article: {
            className: 'timeline-entry begin',
            entry: {
                className: 'timeline-entry-inner',
                icon: {
                    commonClassName: 'timeline-icon',
                    className: '',
                    item: {
                        className: '',
                    }
                },
                label: {
                    className: 'timeline-label',
                    classNameLast: 'LastBlock',
                    hiddenLast: false,
                    id: 'Customer',
                    updateDate: {
                        id: "updateDate",
                        text: ''
                    },
                    owner: {
                        id: 'Customer',
                        text: 'You'
                    },
                    textInfo: {
                        className: 'textInfo',
                        id: 'Customer',
                        text: ''
                    },
                    price: {
                        className: 'price',
                        text: '',
                        hidden: false,
                        item: {
                            className: 'fas fa-dollar-sign',
                        }
                    },
                    scheduleDate: {
                        className: 'scheduleDate',
                        text: '',
                        hidden: false,
                        item: {
                            className: 'far fa-calendar-alt',
                        }
                    },
                    buttonAccept: {
                        id: "ButtonAccept",
                        text: "Accept",
                        hidden: false,
                    },
                    buttonChange: {
                        id: "btn1",
                        text: "Change",
                        hidden: false
                    },
                    buttonCancel: {
                        id: "ButtonCancel",
                        text: "Cancel",
                        hidden: false
                    }
                }
            }
        }
    }
}

function ShowMessages(orderId, requestId) {
    var lengthOfOrderHistory;
    var lastJSON;
    $.ajax({
        type: "GET",
        url: "/api/OrderHistoryAPI/GetAll/?id=" + orderId,
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        dataType: "json",
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (result) {
            lengthOfOrderHistory = result;
            $(".timeline-centered").empty().append('<input type="hidden" id="ChangingFormOrderId" value=' + orderId + '>');
            lastJSON = result[0];
            $(result).each(function (i, item) {
                var rootElement = createDomElements();
                rootElement.article.entry.label.price.text = item.Price;
                rootElement.article.entry.label.scheduleDate.text =new Date(item.ScheduleDate).toUTCString().replace('GMT', '');
                rootElement.article.entry.label.updateDate.text = new Date(item.UpdateDate).toUTCString().replace('GMT', '');

                if (item.TypeOfUser == "Customer") {
                    rootElement.article.entry.label.id = "Company";
                    rootElement.article.entry.label.owner.id = "Company";
                    rootElement.article.entry.label.owner.text = item.NameUser;
                    rootElement.article.entry.label.textInfo.id = 'Company';
                }
                else {

                }
                var resultAfterStatus;
                switch (item.Status) {
                    case (OrderHistoryStatus.Pending):
                        resultAfterStatus = blockWithPendingStatus(rootElement, item);
                        break;
                    case (OrderHistoryStatus.Accepted):
                        resultAfterStatus = blockWithAcceptedStatus(rootElement, item);
                        break;

                    case (OrderHistoryStatus.Changed):
                        resultAfterStatus = blockWithChangedStatus(rootElement, item);
                        break;

                    case (OrderHistoryStatus.Canceled):
                        resultAfterStatus = blockWithCanceledStatus(rootElement, item);
                        break;
                }
                var addButtons;
                var article
                var idOrderAccepted = GetOrderIdAccepted(requestId);
                idOrderAccepted.responseJSON;
                if (i == 0 && idOrderAccepted.responseJSON == -1) {
                    resultAfterStatus.article.entry.label.hiddenLast = true;
                    addButtons = addToBlockButtons(resultAfterStatus.article, item);
                    article = createArticleHtml(addButtons);
                }
                else if (i == 0 && idOrderAccepted.responseJSON == orderId) {
                    resultAfterStatus.article.entry.label.hiddenLast = true;
                    addButtons = addToBlockButtons(resultAfterStatus.article, item);
                    article = createArticleHtml(addButtons);
                }
                else {
                    article = createArticleHtml(resultAfterStatus.article);
                }

                $("div.timeline-centered").append(article);
            })
            //setInterval('btnMessages.click()', 10000);
        }, error: function () {
           // alert("Messages are not available, try again later");
        }
    });
    $.ajax({
        type: "GET",
        url: "/api/OrderHistoryAPI/GetRequest/?id=" + requestId,
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        /*data:JSON.stringify(JSONObject),*/
        dataType: "json",
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (result) {
            var rootElement = createDomElements();
            rootElement.article.entry.label.scheduleDate.text =new Date(result.Deadline).toUTCString().replace('GMT', '');
            rootElement.article.entry.label.updateDate.text = new Date(result.Date).toUTCString().replace('GMT', '');
            rootElement.article.entry.label.scheduleDate.hidden = true;
            rootElement.article.entry.label.textInfo.text = "Request was created";
            rootElement.article.entry.label.textInfo
            rootElement.article.entry.icon.className = 'bg-info';
            rootElement.article.entry.icon.item.className = 'fas fa-plus-circle';
            rootElement.article.entry.label.id = "Company";
            rootElement.article.entry.label.owner.id = "Company";
            rootElement.article.entry.label.owner.text = "Customer";
            rootElement.article.entry.label.textInfo.id = 'Company';
            if (lengthOfOrderHistory == 0) {
                rootElement.article.entry.label.buttonChange.hidden = true;
                rootElement.article.entry.label.buttonChange.text = "Cooperation";
                rootElement.article.entry.label.buttonChange.id = "ButtonCooperation";
            }
            var requestCreated = createArticleHtml(rootElement.article);
            var blockMessages = $("div.timeline-centered");

            blockMessages.append(requestCreated);
        }, error: function (error) {
            //alert(error);
        }
    });
    $("#ButtonAccept").click(function () {
        $.ajax({
            url: "/api/OrderHistoryAPI/AddInOrderHistory/?currentStatus=" + 3,
            type: 'POST',
            dataType: 'json',
            data: {
                Status: lastJSON.Status,
                Price: lastJSON.Price,
                ScheduleDate: lastJSON.ScheduleDate,
                UpdateDate: lastJSON.UpdateDate,
                Order: lastJSON.Order,
            },
            headers: {
                "Accept": "application/json",
                "Authorization": "Bearer " + token
            },
            success: function () {
                ShowMessages(orderId, requestId);
            },
            error: function () {
                //alert("Error please try again");
            }
        });
    })

    $("#ButtonCancel").click(function () {
        $.ajax({
            url: "/api/OrderHistoryAPI/AddInOrderHistory/?currentStatus=" + 4,
            type: 'POST',
            dataType: 'json',
            headers: {
                "Accept": "application/json",
                "Authorization": "Bearer " + token
            },
            data: {
                Status: lastJSON.Status,
                Price: lastJSON.Price,
                ScheduleDate: lastJSON.ScheduleDate,
                UpdateDate: lastJSON.UpdateDate,
                Order: lastJSON.Order,
            },
            success: function () {
                flag = false;
                ShowMessages(orderId, requestId);
            },
            error: function () {
                //alert("Error please try again");
            }
        });
    })

    $("#ButtonCooperation").click(function () {
        $.ajax({
            url: "/api/OrderHistoryAPI/AddInOrderHistoryCooperation/?orderId=" + orderId,
            type: 'POST',
            dataType: 'json',
            headers: {
                "Accept": "application/json",
                "Authorization": "Bearer " + token
            },
            success: function () {
                flag = false;
                ShowMessages(orderId, requestId);
                //alert("Saved Successfully");
            },
            error: function () {
                //alert("Error please try again");
            }
        });
    })

    //chekOrder = orderId;
}
function blockWithAcceptedStatus(objectOrderHistory, item) {
    objectOrderHistory.article.entry.icon.className = 'bg-success';
    switch (item.Changed) {
        case ("Price"): {
            objectOrderHistory.article.entry.icon.item.className = 'fas fa-dollar-sign';
            objectOrderHistory.article.entry.label.price.hidden = true;
            if (item.TypeOfUser == "Company") {

                objectOrderHistory.article.entry.label.textInfo.text = item.NameUser + " change price request was confirmed";
            }
            else {

                objectOrderHistory.article.entry.label.textInfo.text = "Your change price request was confirmed";
            }
            break;
        }
        case ("DateTime"): {
            objectOrderHistory.article.entry.icon.item.className = "far fa-calendar-alt";
            objectOrderHistory.article.entry.label.scheduleDate.hidden = true;
            if (item.TypeOfUser == "Company") {
                objectOrderHistory.article.entry.label.textInfo.text = item.NameUser + " change date/time request was confirmed";
            }
            else {

                objectOrderHistory.article.entry.label.textInfo.text = "Your change date/time request was confirmed";
            }
            break;
        }
        case ("All"): {
            objectOrderHistory.article.entry.icon.item.className = "fas fa-check-circle";
            objectOrderHistory.article.entry.label.price.hidden = true;
            objectOrderHistory.article.entry.label.scheduleDate.hidden = true;
            if (item.TypeOfUser == "Company") {
                objectOrderHistory.article.entry.label.textInfo.text = item.NameUser + " change date/time and price request was confirmed";
            }
            else {
                objectOrderHistory.article.entry.label.textInfo.text = "Your change date/time and price request was confirmed";

            }
            break;
        }

    }
    return objectOrderHistory;
}

function blockWithChangedStatus(objectOrderHistory, item) {
    objectOrderHistory.article.entry.icon.className = 'bg-info';
    switch (item.Changed) {
        case ("Price"): {
            objectOrderHistory.article.entry.icon.item.className = 'fas fa-dollar-sign';
            objectOrderHistory.article.entry.label.price.hidden = true;
            objectOrderHistory.article.entry.label.textInfo.text = "Request to change price";
            break;

        }
        case ("DateTime"): {
            objectOrderHistory.article.entry.icon.item.className = "far fa-calendar-alt";
            objectOrderHistory.article.entry.label.scheduleDate.hidden = true;
            objectOrderHistory.article.entry.label.textInfo.text = "Request to change date/time";
            break;
        }
        case ("All"):
            {
                objectOrderHistory.article.entry.label.price.hidden = true;
                objectOrderHistory.article.entry.label.scheduleDate.hidden = true;
                objectOrderHistory.article.entry.icon.item.className = "fas fa-check-circle";
                objectOrderHistory.article.entry.label.textInfo.text = "Request to change price and date/time";
                break;
            }
    }

    return objectOrderHistory;
}

function blockWithCanceledStatus(objectOrderHistory, item) {
    objectOrderHistory.article.entry.icon.className = 'red';
    objectOrderHistory.article.entry.label.price.hidden = true;
    objectOrderHistory.article.entry.label.scheduleDate.hidden = true;
    objectOrderHistory.article.entry.icon.item.className = 'fas fa-exclamation-circle';
    objectOrderHistory.article.entry.label.textInfo.text = "Declined to work on current agreements";
    return objectOrderHistory;
}
function blockWithPendingStatus(objectOrderHistory, item) {
    objectOrderHistory.article.entry.icon.className = 'bg-warning';
    objectOrderHistory.article.entry.label.price.hidden = true;
    objectOrderHistory.article.entry.label.scheduleDate.hidden = true;
    objectOrderHistory.article.entry.icon.item.className = 'far fa-lightbulb';
    objectOrderHistory.article.entry.label.textInfo.text = "Ready to work in your request";
    return objectOrderHistory;
}

function addToBlockButtons(objectOrderHistory, item) {
    if (item.TypeOfUser == "Company")
        switch (item.Status) {
            case (OrderHistoryStatus.Pending): {
                objectOrderHistory.entry.label.buttonChange.hidden = true;
                objectOrderHistory.entry.label.buttonCancel.hidden = true;
                break;
            }
            case (OrderHistoryStatus.Canceled):
                {
                    objectOrderHistory.entry.label.buttonChange.hidden = true;
                    break;
                }
            case (OrderHistoryStatus.Accepted):
                {
                    objectOrderHistory.entry.label.buttonChange.hidden = true;
                    objectOrderHistory.entry.label.buttonCancel.hidden = true;
                    break;
                }
            case (OrderHistoryStatus.Changed):
                {
                    objectOrderHistory.entry.label.buttonChange.hidden = true;
                    objectOrderHistory.entry.label.buttonCancel.hidden = true;
                    break;
                }
        }
    else if (item.TypeOfUser == "Customer") {
        switch (item.Status) {
            case (OrderHistoryStatus.Changed): {
                objectOrderHistory.entry.label.buttonAccept.hidden = true;
                objectOrderHistory.entry.label.buttonChange.hidden = true;
                objectOrderHistory.entry.label.buttonCancel.hidden = true;
                break;
            }
            case (OrderHistoryStatus.Canceled):
                {
                    objectOrderHistory.entry.label.buttonChange.hidden = true;
                    break;
                }
            case (OrderHistoryStatus.Accepted):
                {
                    objectOrderHistory.entry.label.buttonChange.hidden = true;
                    objectOrderHistory.entry.label.buttonCancel.hidden = true;
                    break;
                }
        }
    }
    return objectOrderHistory;
}
function createArticleHtml(article) {
    //items for creating message blocks
    var timelineEntry = $('<article>');
    timelineEntry.addClass(article.className);

    var timelineEntryInner = $('<div>');
    timelineEntryInner.addClass(article.entry.className);
    timelineEntry.append(timelineEntryInner);


    var icon = $('<div>');
    icon.addClass(article.entry.icon.commonClassName + " " + article.entry.icon.className);
    var symbolIcon = $('<i>');
    symbolIcon.addClass(article.entry.icon.item.className);
    icon.append(symbolIcon);
    timelineEntryInner.append(icon);

    var timelineLabel = $('<div>');
    timelineLabel.addClass(article.entry.label.className);
    timelineLabel.attr('id', article.entry.label.id);
    timelineEntryInner.append(timelineLabel);

    var owner = $('<span>');
    owner.attr('id', article.entry.label.owner.id);
    owner.text(article.entry.label.owner.text);
    timelineLabel.append(owner);

    var updateDate = $('<span>');
    updateDate.attr('id', article.entry.label.updateDate.id);
    updateDate.text(article.entry.label.updateDate.text);
    timelineLabel.append(updateDate);

    var textInfo = $('<p>');
    textInfo.addClass(article.entry.label.textInfo.className);
    textInfo.attr('id', article.entry.label.textInfo.id);
    textInfo.text(article.entry.label.textInfo.text);
    timelineLabel.append(textInfo);

    if (article.entry.label.price.hidden == true) {
        var price = $('<span>');
        price.addClass(article.entry.label.price.className);
        var iconPrice = $('<i>');
        iconPrice.text(" " + article.entry.label.price.text);
        iconPrice.addClass(article.entry.label.price.item.className);
        price.append(iconPrice);
        timelineLabel.append(price);
    }

    if (article.entry.label.scheduleDate.hidden == true) {
        var scheduleDate = $('<span>');
        scheduleDate.addClass(article.entry.label.scheduleDate.className);
        var iconDate = $('<i>');
        iconDate.text(" " + article.entry.label.scheduleDate.text);
        iconDate.addClass(article.entry.label.scheduleDate.item.className);
        scheduleDate.append(iconDate);
        timelineLabel.append(scheduleDate);
    }
    //items for button
    var buttonAccept = $('<button>');
    buttonAccept.text(article.entry.label.buttonAccept.text);
    buttonAccept.attr("id", article.entry.label.buttonAccept.id);
    var buttonChange = $('<button>');
    buttonChange.attr('id', article.entry.label.buttonChange.id);
    buttonChange.attr('data-toggle', 'collapse');
    buttonChange.attr('href', '#multiCollapseExample');
    buttonChange.attr('aria-expanded', 'false');
    buttonChange.attr('role', 'button');
    buttonChange.attr('aria-controls', 'multiCollapseExample');
    buttonChange.text(article.entry.label.buttonChange.text);
    var buttonCancel = $('<button>');
    buttonCancel.text(article.entry.label.buttonCancel.text);
    buttonCancel.attr("id", article.entry.label.buttonCancel.id);
    timelineLabel.append("<br>")

    if (article.entry.label.hiddenLast == true)
        timelineLabel.addClass(article.entry.label.classNameLast);

    if (article.entry.label.buttonAccept.hidden == true)

        timelineLabel.append(buttonAccept);
    if (article.entry.label.buttonChange.hidden == true)

        timelineLabel.append(buttonChange);
    if (article.entry.label.buttonCancel.hidden == true)
        timelineLabel.append(buttonCancel);

    var formChange = $('<div>');
    formChange.attr('id', 'was');

    timelineEntryInner.append(formChange);
    return timelineEntry;
}

function CreateMainBlock() {
    var containerForEmpty = $("#addRow");
    var rootElement = createMainDomElements();
    var mainContainer = $("<div>");
    mainContainer.addClass(rootElement.mainContainer.className);
    mainContainer.attr("id", rootElement.mainContainer.id);
    containerForEmpty.append(mainContainer);
    var orderHistoryList = $("<div>");
    orderHistoryList.addClass(rootElement.mainContainer.orderHistoryList.className);
    orderHistoryList.attr("id", rootElement.mainContainer.orderHistoryList.id);
    mainContainer.append(orderHistoryList);
    var blockMessages = $("<div>");
    blockMessages.addClass(rootElement.mainContainer.orderHistoryList.blockMessages.className);
    orderHistoryList.append(blockMessages);
}

function GetOrderIdAccepted(requestId) {
    return $.ajax({
        type: "GET",
        url: "/api/OrderHistoryAPI/СheckIfThereIsStatusAcceptedOrArchived/?idRequest=" + requestId,
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        /*data:JSON.stringify(JSONObject),*/
    });
}
