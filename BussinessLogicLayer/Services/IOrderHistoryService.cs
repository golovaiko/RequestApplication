﻿using BussinessLogicLayer.Models;
using BussinessLogicLayer.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Services
{
    public interface IOrderHistoryService
    {
        Request GetRequest(int id);
        IEnumerable<OrderHistory> Find(int orderId);
        Order GetOrder(int id);
        Customer GetCustomer(string id);
        Company GetCompany(string id);
        InformationAboutUser AdditionInfoAboutUser(string id);
        void AddOnlyWithAC(OrderHistoryDTO orderHistory, int newStatus);
        int CheckIfAgreed(int idRequest);
        OrderHistory GetOrderHistoryById(int id);
        Request GetCurrentRequestForOrderHistory(OrderHistoryDTO item);
    }
}
