﻿using BussinessLogicLayer.Core;
using BussinessLogicLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Repositories
{
    public interface ICategoryRepository : IRepository<Category, int>
    {

    }
}
