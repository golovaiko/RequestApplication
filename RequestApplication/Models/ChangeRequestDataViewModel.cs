﻿using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;

namespace RequestApplication.Models
{
    public class ChangeRequestDataViewModel : IValidatableObject
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime ScheduleDate { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        [DataType(DataType.Currency)]
        public string Price { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            if (ScheduleDate <= DateTime.Now && ScheduleDate >= DateTime.Now.AddMonths(1))
            {
                results.Add(new ValidationResult("Start date and time must be greater than current time", new[] { "Date" }));
            }
            return results;
        }
    }
}