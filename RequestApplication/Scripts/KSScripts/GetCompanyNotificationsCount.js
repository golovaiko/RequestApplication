﻿var stopInterval;
$(document).ready(function () {
    GetCompanyActiveNotifications();
    setInterval(GetCompanyActiveNotifications, 5000);
    stopInterval = setInterval(GetCompanyNotifications, 5000);
});

$(document).on('click', "#NotificationLink", function () {
    clearInterval(stopInterval);
});
$(document).on('click', "#buttonClose", function () {
    stopInterval = setInterval(GetCompanyNotifications, 5000);
});


function GetCompanyActiveNotifications() {
    $.ajax({
        url: '/api/NotificationsAPI/GetCompanyNotifications',
        type: 'GET',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        dataType: 'json',
        success: function (data) {
            var count = 0;
            $(data).each(function (key, instance) {
                if (instance.IsShown == false) {
                    count++;
                }

            });
            $('#notificationBadge').text(count);
        },
        error: function () {
            //alert('Error!');
        }
    });
}
