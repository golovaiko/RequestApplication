﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Models
{
    public class InformationAboutUser
    {
         public string TypeOfUser { get; set; }
        public  string NameOfUser { get; set; }
    }
}
