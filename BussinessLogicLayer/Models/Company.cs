﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Models
{
    public class Company
    {
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        public byte[] ProfileImage { get; set;
        }
        public string PhoneNumber { get; set; }

        public ICollection<CompanyOffer> CompanyOffers { get; set; }
    }
}