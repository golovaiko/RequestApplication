﻿$("div").on('mouseover', '.falseAction', function () {
    $(this).addClass('card');
    var id = $(this).find('input#hiddenId').val();
    $(this).find('a').removeClass('list-group-item-action');
    $(this).find('a').removeClass('newNotification');
    $(this).find('a').addClass('trueNotification');
    $(this).removeClass('falseAction');
    $(this).addClass('trueAction');
    $(this).find('#dollarSign').addClass('dollarSign');
    $(this).find('#userSign').addClass('userSign');
    $(this).find('#calendarSign').addClass('calendarSign');
    $(this).find('h5 .badge').remove();
    var orderId = $(this).find('input#hiddenOrderId').val();
    var OpenedChatId = $('#ChangingFormOrderId').val();
    if (orderId != OpenedChatId) {
        $('div#tabsOrderHistory').remove();
    }
    var apiurl = "/api/NotificationsAPI/isActive?id=" + id;
    $.ajax({
        url: apiurl,
        type: 'POST',
        dataType: 'json',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function () {

        },
        error: function () {
        }
    });
    GetCompanyActiveNotifications();
});

$(document).on('click', '.trueAction', function () {
    var orderId = $(this).find('input#hiddenOrderId').val();
    var ChatOrderId = $("#ChangingFormOrderId").val();
    if (ChatOrderId != orderId) {
        $('div#tabsOrderHistory').remove();
        $("[data-order-id=" + "'" + orderId + "']").click();
        $('#buttonClose').click();
    }
    else {
        $('#buttonClose').click();
    }
  
});


$(document).on('mouseover', '.trueAction', function () {
    $(this).find('a').removeClass('trueNotification');
    $(this).find('a').addClass('trueNotificationOnMO');
});

$(document).on('mouseout', '.trueAction', function () {
    $(this).find('a').removeClass('trueNotificationOnMO');
    $(this).find('a').addClass('trueNotification');
});

$(document).on('click', '.createChat', function () {
    var notificationsCount = $('#notificationBadge').text();
    if (notificationsCount != 0) {
        var orderId = $(this).attr('data-order-id');
        var apiurl = "/api/NotificationsAPI/DeleteOpenedChatNotifications?orderId=" + orderId;
        $.ajax({
            url: apiurl,
            type: 'POST',
            dataType: 'json',
            headers: {
                "Accept": "application/json",
                "Authorization": "Bearer " + token
            }
        });
        GetActiveNotifications();
    }
});

