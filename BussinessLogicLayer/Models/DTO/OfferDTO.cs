﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Models.DTO
{
    public class OfferDTO
    {
        [Required]
        public Offer Offer { get; set; }

        [Required]
        public bool IsProviding { get; set; }

        public string OfferPrice { get; set; }
    }
}
