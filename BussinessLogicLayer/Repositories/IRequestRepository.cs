﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessLogicLayer.Core;
using BussinessLogicLayer.Models;

namespace BussinessLogicLayer.Repositories
{
    public interface IRequestRepository : IRepository<Request, int>
    {
        IEnumerable<Request> GetByUserId(string userId);
        Offer GetOffer(int id);
        IEnumerable<Request> GetAllWithCustomer(Func<Request, bool> predicate);
        IEnumerable<int> GetRequestsStatusByUserId(string UserId);
        Request GetLastRequest();
        List<Request> GetRequestByIdAndStatus(string userId, int status);
        List<Request> GetRequestsByOrdersIds(IEnumerable<int> OrdersIds);

    }
}
