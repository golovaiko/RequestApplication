﻿var token = sessionStorage.getItem('tokenKey');
$('#buttonLaunch').click(function () {
    setDateConstraintsForDatePicker();
    resetValidationStyles();
    $('#requestCreationPopupBody').trigger('reset');
    OfferAjax('/API/RequestAPI/GetOffers?categoryId=', $('#categoryDropDownList').val());
})

CategoryOfferAjax('/API/RequestAPI/GetCategories');

$('#categoryDropDownList').change(function () {
    OfferAjax('/API/RequestAPI/GetOffers?categoryId=', $('#categoryDropDownList').val())
});

$("#createRequestButton").click(function () {
    resetValidationStyles();
    CreateNewRequest('/API/RequestAPI/CreateNewRequest');
});

function CategoryOfferAjax(url) {
    $.ajax({
        url: url,
        type: 'GET',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (categories) {
            $('#categoryDropDownList').html('');
            var categoryOptions = '';
            for (var i = 0; i < categories.length; i++) {
                categoryOptions += '<option value=' + categories[i].Id + '>' + categories[i].Name + '</option>';
            }
            $('#categoryDropDownList').append(categoryOptions);
            $.ajax({
                url: '/API/RequestAPI/GetOffers?categoryId=' + Number($('#categoryDropDownList  ').val()),
                type: 'GET',
                headers: {
                    "Accept": "application/json",
                    "Authorization": "Bearer " + token
                },
                success: function (offers) {
                    if (offers.length > 0) {
                        $('#offerDropDownList').html('');
                        var offerOptions = '';
                        for (var i = 0; i < offers.length; i++) {
                            offerOptions += '<option value=' + offers[i].Id + '>' + offers[i].Name + '</option>';
                        }
                        $('#offerDropDownList').append(offerOptions);
                    }
                },
                error: function (error) { console.log(error) }
            });
        },
        error: function (error) { console.log(error) }
    });
}

function OfferAjax(url, neededId) {
    $.ajax({
        url: url + Number(neededId),
        type: 'GET',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (offers) {
            if (offers.length > 0) {
                $('#offerDropDownList').html('');
                var offerOptions = '';
                for (var i = 0; i < offers.length; i++) {
                    offerOptions += '<option value=' + offers[i].Id + '>' + offers[i].Name + '</option>';
                }
                $('#offerDropDownList').append(offerOptions);
            }
        },
        error: function (error) { console.log(error) }
    });
}

function CreateNewRequest(url) {
    if (validateMinPriceTextBox && validateMaxPriceTextBox) {
        if (Number.parseInt($("#minPriceTextBox").val()) >= Number.parseInt($("#maxPriceTextBox").val())) {
            $("#invalidPrice").show();
            return;
        }

        if (!validateTitleTextBox() || !validateDateTimeTextBox()) {
            $("#invalidData").show();
            return;
        }
    }

    $("#invalidData").hide();
    $("#invalidPrice").hide();

    $.ajax({
        url: url,
        type: 'POST',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        data: {
            Title: $("#titleTextBox").val().trim(),
            Description: $("#descriptionTextBox").val(),
            MinPrice: $("#minPriceTextBox").val().trim(),
            MaxPrice: $("#maxPriceTextBox").val().trim(),
            Deadline: $("#deadlineTextBox").val(),
            OfferId: $('#offerDropDownList').val(),
            CustomerId: ''
        },
        dataType: "json",
        success: function () {
            $.ambiance({message: "Created successfull!",
                title: "Success!",
                type: "success"
            })
            $("#closeRequestCreationModal").click();
            $("#requestCreationPopupBody").trigger('reset');
            OfferAjax('/API/RequestAPI/GetOffers?categoryId=', $('#categoryDropDownList').val());
            $("#allList").empty();
            $("h5#TextMyRequest").empty();
            GetRequestsForCurrentUserByStatus(RequestStatus.All);
        },
        error: function () {
            $.ambiance({
                message: "Error try again please!",
                title: "Error!",
                type: "error"
            })
            $("#closeRequestCreationModal").click();
        }
    });
}