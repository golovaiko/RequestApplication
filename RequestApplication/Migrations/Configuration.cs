namespace RequestApplication.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RequestApplication.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RequestApplication.Models.ApplicationDbContext context)
        {
            context.Roles.AddOrUpdate(x => x.Id,
                new IdentityRole() { Id = "1", Name = "customer" },
                new IdentityRole() { Id = "2", Name = "company" });
        }
    }
}
