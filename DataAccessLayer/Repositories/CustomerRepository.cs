﻿using BussinessLogicLayer.Models;
using BussinessLogicLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class CustomerRepository : Repository<Customer, string>, ICustomerRepository
    {
        public CustomerRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public List<string> GetValidCustomerIds(List<string> customerIds)
        {
            return UnitOfWork.Context.Customers
                .Where(x => customerIds.Contains(x.Id))
                .Select(x => x.Id)
                .ToList();
        }

        public IEnumerable<Customer> GetCustomersFromOrderHistories(IEnumerable<string> CustomerIds)
        {
            return UnitOfWork.Context.Customers
                .Where(x => CustomerIds.Contains(x.Id))
                .ToList();
        }

        
    }
}
