﻿using BussinessLogicLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RequestApplication.Models
{
    public class CurrentOrderHistoryVM
    {
        public int Id { get; set; }
        public DateTime ScheduleDate { get; set; }

        public string Price { get; set; }
        
        public string UserType { get; set; }

        public static CurrentOrderHistoryVM Init(OrderHistory orderHistory, InformationAboutUser informationAboutUser)
        {
            return new CurrentOrderHistoryVM()
            {
                Id = orderHistory.Id,
                Price = orderHistory.Price,
                ScheduleDate = orderHistory.ScheduleDate,
                UserType=informationAboutUser.TypeOfUser
            };
        }
    }
}