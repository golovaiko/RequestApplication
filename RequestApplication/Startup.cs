﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RequestApplication.Startup))]
namespace RequestApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            
        }
    }
}
