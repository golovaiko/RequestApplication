﻿using System.Data.Entity;
using BussinessLogicLayer.Models;

namespace DataAccessLayer.Context
{
    public class RequestDBContext : DbContext
    {
        public RequestDBContext() : base("RequestDBContext")
        {

        }
        public DbSet<Request> Requests { get; set; }        
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<CompanyOffer> CompanyOffers { get; set; }
        public DbSet<OrderHistory> OrderHistories { get; set; }
        public DbSet<UsersFeedback> UsersFeedbacks { get; set; }
    }
}
