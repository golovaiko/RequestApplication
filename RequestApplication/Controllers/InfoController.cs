﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RequestApplication.Controllers
{
    public class InfoController : Controller
    {
        // GET: Info
        public ActionResult OurTeam()
        {
            return View();
        }

        public ActionResult FAQ()
        {
            return View();
        }

        public ActionResult ContactUs()
        {
            return View();
        }

        public ActionResult News()
        {
            return View();
        }
    }
}