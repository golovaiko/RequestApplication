﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Core
{
    public interface IRepository<T, TId> where T : class
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(Expression<Func<T, bool>> filter = null);
        T GetById(TId id);
        void Create(T entity);
        void Update(T entity);
        void Delete(T id);
        void SaveChanges();
        void Dispose();
    }
}
