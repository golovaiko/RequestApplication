﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Models.DTO
{
    public class CustomerUpdateProfileDataDTO
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string Gender { get; set; }

        [Required]
        public string BirthDate { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        public string UserId { get; set; }

        public byte[] ProfileImage { get; set; }
    }
}
