﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace BussinessLogicLayer.Models
{
    public class Order
    {
        public int Id { get; set; }
        public int RequestId { get; set; }
        public string CompanyId { get; set; }

        [Required]
        public string Price { get; set; }

        [Required]
        public DateTime ExecutionDate { get; set; }

        [JsonIgnore]
        public ICollection<OrderHistory> OrderHistories { get; set; }
    }
}
