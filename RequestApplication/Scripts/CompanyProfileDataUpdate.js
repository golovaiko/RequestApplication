﻿var token = sessionStorage.getItem('tokenKey');

$("#saveCompanyProfileChanges").click(function () {
    UpdateCompanyDataAjax();
})

GetCompanyDataAjax();

function GetCompanyDataAjax() {
    $.ajax({
        url: '/API/UserDataUpdateAPI/GetCompanyProfileData',
        type: 'GET',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (user) {
            $("#companyNameTextBox").val(user.Name),
                $("#companyAddressTextBox").val(user.Address),
                $("#companyPhoneNumberTextBox").val(user.PhoneNumber),
                $("#companyNameLabel").text(user.Name)
        },
        error: function (error) { console.log(error) }
    });
}

function UpdateCompanyDataAjax() {
    if (!Number.parseInt($("#companyPhoneNumberTextBox").val())) {
        $("#invalidCompanyPhoneNumber").show();
        return;
    }

    if (!validateCompanyNameTextBox() || !validateCompanyAddressTextBox()) {
        $("#invalidCompanyNameOrAddress").show();
        return;
    }

    $("#invalidCompanyNameOrAddress").hide();
    $("#invalidCompanyPhoneNumber").hide();

    $.ajax({
        url: '/API/UserDataUpdateAPI/UpdateCompanyProfileData',
        type: "POST",
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        data: {
            Name: $("#companyNameTextBox").val(),
            Address: $("#companyAddressTextBox").val(),
            PhoneNumber: $("#companyPhoneNumberTextBox").val(),
            UserId: ''
        },
        dataType: "json",
        success: function () {
            $.ambiance({
                message: "Saved successfull!",
                title: "Success!",
                type: "success"
            })
            $("#closeUpdateCompanyProfileModal").click();
        },
        error: function () {
            $.ambiance({
                message: "Error try again please!",
                title: "Error!",
                type: "error"
            })
            $("#closeUpdateCompanyProfileModal").click();
        }
    });

}