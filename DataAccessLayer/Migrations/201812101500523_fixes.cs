namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CompanyOffers", "OfferPrice", c => c.String());
            AlterColumn("dbo.Requests", "Title", c => c.String());
            AlterColumn("dbo.Requests", "Description", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Requests", "Description", c => c.String(nullable: false));
            AlterColumn("dbo.Requests", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.CompanyOffers", "OfferPrice", c => c.String(nullable: false));
        }
    }
}
