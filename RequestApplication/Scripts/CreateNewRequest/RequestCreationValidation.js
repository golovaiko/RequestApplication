﻿$("#titleTextBox").change(function () {
    validateTitleTextBox();
});
$("#titleTextBox").focusout(function () {
    validateTitleTextBox();
});


$("#minPriceTextBox").change(function () {
    validateMinPriceTextBox();
});
$("#minPriceTextBox").focusout(function () {
    validateMinPriceTextBox();
});


$("#maxPriceTextBox").change(function () {
    validateMaxPriceTextBox();
});
$("#maxPriceTextBox").focusout(function () {
    validateMaxPriceTextBox();
});


$("#deadlineTextBox").change(function () {
    validateDateTimeTextBox();
});
$("#deadlineTextBox").focusout(function () {
    validateDateTimeTextBox();
});

$('#descriptionTextBox').change(function () {
    validateDescriptionTextArea();
})
$('#descriptionTextBox').focusout(function () {
    validateDescriptionTextArea();
})

function resetValidationStyles() {
    $("#titleTextBox").removeClass('errorInput');
    $("#minPriceTextBox").removeClass('errorInput');
    $("#maxPriceTextBox").removeClass('errorInput');
    $("#deadlineTextBox").removeClass('errorInput');
    $("#descriptionTextBox").removeClass('errorTextArea');

    $("#invalidData").hide();
    $("#invalidPrice").hide();
}

function setDateConstraintsForDatePicker() {
    var date = new Date();
    var minDate = new Date(date.setTime(date.getTime() + 1 * 86400000));

    var minMonth = minDate.getMonth() + 1;
    var minDay = minDate.getDate();
    var minYear = minDate.getFullYear();

    if (minMonth < 10)
        minMonth = '0' + minMonth.toString();
    if (minDay < 10)
        minDay = '0' + minDay.toString();

    var minDateResult = minYear + '-' + minMonth + '-' + minDay;
    $('#deadlineTextBox').attr('min', minDateResult);

    date = new Date();
    var maxDate = new Date(date.setTime(date.getTime() + 30 * 86400000));

    var maxMonth = maxDate.getMonth() + 1;
    var maxDay = maxDate.getDate();
    var maxYear = maxDate.getFullYear();
    if (maxMonth < 10)
        maxMonth = '0' + maxMonth.toString();
    if (maxDay < 10)
        maxDay = '0' + maxDay.toString();

    var maxDateResult = maxYear + '-' + maxMonth + '-' + maxDay;
    $('#deadlineTextBox').attr('max', maxDateResult);
}

function validateTitleTextBox() {
    let textBox = $("#titleTextBox");
    if (textBox.val() === null || textBox.val().trim() === '') {
        textBox.addClass('errorInput');
        return false;
    }
    else {
        textBox.removeClass('errorInput');
        return true;
    }
}

function CheckIfAllDigits(value) {
    return /^\d+$/.test(value);
}

function validateMinPriceTextBox() {
    let minPriceTextBox = $("#minPriceTextBox");

    if (CheckIfAllDigits(minPriceTextBox.val()) && Number.parseInt(minPriceTextBox.val()) > 0) {
        minPriceTextBox.removeClass('errorInput');
        return true;
    }
    else {
        minPriceTextBox.addClass('errorInput');
        return false;
    }
}

function validateMaxPriceTextBox() {
    let maxPriceTextBox = $("#maxPriceTextBox");

    if (CheckIfAllDigits(maxPriceTextBox.val()) && Number.parseInt(maxPriceTextBox.val()) > 0) {
        maxPriceTextBox.removeClass('errorInput');
        return true;
    }
    else {
        maxPriceTextBox.addClass('errorInput');
        return false;
    }
}

function validateDateTimeTextBox() {
    let deadlineTextBox = $("#deadlineTextBox");

    var date = new Date();
    var minDate = new Date(date.setTime(date.getTime() + 1 * 86400000)).setHours(0, 0, 0, 0);

    var date2 = new Date();
    var maxDate = new Date(date2.setTime(date2.getTime() + 30 * 86400000)).setHours(0, 0, 0, 0);

    if (deadlineTextBox.val() === ""
        || new Date(deadlineTextBox.val()).setHours(0, 0, 0, 0) < minDate
        || new Date(deadlineTextBox.val()).setHours(0, 0, 0, 0) > maxDate) {
        deadlineTextBox.addClass('errorInput');
        return false;
    }
    else {
        deadlineTextBox.removeClass('errorInput');
        return true;
    }
}

function validateDescriptionTextArea() {
    let descriptionTextArea = $('#descriptionTextBox');
    if (descriptionTextArea.val() === null || descriptionTextArea.val().trim() === '') {
        descriptionTextArea.addClass('errorInput');
        return false;
    }
    else {
        descriptionTextArea.removeClass('errorInput');
        return true;
    }
}