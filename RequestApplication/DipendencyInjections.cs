﻿using BussinessLogicLayer.Repositories;
using DataAccessLayer.Repositories;
using Ninject;
using BussinessLogicLayer.Services;
using BussinessLogicLayer.Core;
using DataAccessLayer;

namespace RequestApplication
{
    public class DipendencyInjections
    {
        public static IKernel Initialize(IKernel kernel)
        {

            kernel.Bind<ICategoryRepository>().To<CategoryRepository>();
            kernel.Bind<ICompanyRepository>().To<CompanyRepository>();
            kernel.Bind<ICustomerRepository>().To<CustomerRepository>();
            kernel.Bind<IOrderRepository>().To<OrderRepository>();
            kernel.Bind<IOfferRepository>().To<OfferRepository>();
            kernel.Bind<ICompanyOfferRepository>().To<CompanyOfferRepository>();
            kernel.Bind<IOrderHistoryRepository>().To<OrderHistoryRepository>();
            kernel.Bind<IRequestRepository>().To<RequestRepository>();
            //kernel.Bind<IUnitOfWork>().To<UnitOfWork>();

            //services
            kernel.Bind<IOrderHistoryService>().To<OrderHistoryService>();

            //kernel.Bind<IApplicationUserRepository>().To<ApplicationUserRepository>();

            //kernel.Bind<ApplicationDbContext>().ToSelf().InRequestScope();
            //kernel.Bind<IUserStore<ApplicationUser>>().To<ApplicationUserStore>();
            //kernel.Bind<ApplicationUserManager>().ToSelf();
            //kernel.Bind<ApplicationSignInManager>().ToSelf();
            //kernel.Bind<IAuthenticationManager>().ToMethod(x => HttpContext.Current.GetOwinContext().Authentication);
            //kernel.Bind<IDataProtectionProvider>().ToMethod(x => Startup.AppBuilder.GetDataProtectionProvider());
            return kernel;
        }
    }
}