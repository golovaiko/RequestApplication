﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessLogicLayer.Models;


namespace BussinessLogicLayer.Services
{
    public interface IRequestStatusCountService
    {
        Dictionary<string, int> GetRequestsStatusCount(string UserId);

    }
}
