namespace DataAccessLayer.Migrations
{
    using BussinessLogicLayer.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataAccessLayer.Context.RequestDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Context.RequestDBContext context)
        {
            context.Categories.AddOrUpdate(x => x.Id,
                new Category() { Id = 1, Name = "Transport" },
                new Category() { Id = 2, Name = "Animal" },
                new Category() { Id = 3, Name = "Sport" },
                new Category() { Id = 4, Name = "Work" });

            context.Offers.AddOrUpdate(x => x.Id,
                new Offer() { Id = 1, Name = "Car Repair", CategoryId = 1 },
                new Offer() { Id = 2, Name = "Car Wash", CategoryId = 1 },
                new Offer() { Id = 3, Name = "Cat Haircut", CategoryId = 2 },
                new Offer() { Id = 4, Name = "Dog Walk", CategoryId = 2 },
                new Offer() { Id = 5, Name = "Play Football", CategoryId = 3 },
                new Offer() { Id = 6, Name = "Running", CategoryId = 3 },
                new Offer() { Id = 7, Name = "Work Searching", CategoryId = 4 },
                new Offer() { Id = 8, Name = "Work Giving", CategoryId = 4 }
               );
        }
    }
}
