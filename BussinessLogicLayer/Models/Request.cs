﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Models
{
    public class Request
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int MinPrice { get; set; }

        public int MaxPrice { get; set; }

        public DateTime? Deadline { get; set; }

        public DateTime Date { get; set; }

        public int Status { get; set; } 

        public string CustomerId { get; set; }

        public int OfferId { get; set; }

        public Customer Customer { get; set; }

        public Offer Offer { get; set; }
    }
}
