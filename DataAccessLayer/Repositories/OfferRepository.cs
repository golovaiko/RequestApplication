﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessLogicLayer.Models;
using BussinessLogicLayer.Repositories;

namespace DataAccessLayer.Repositories
{
    public class OfferRepository : Repository<Offer, int> , IOfferRepository
    {
        public OfferRepository (UnitOfWork unitOfWork) : base (unitOfWork)
        {

        }
    }
}
