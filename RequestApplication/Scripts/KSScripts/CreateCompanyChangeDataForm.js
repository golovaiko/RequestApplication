﻿
$('body').on('click', '#btn1',
    function () {
        if ($("div#multiCollapseExample").length) {
            $("div#multiCollapseExample").remove();
        }

        var FirstDiv = $('<div>');
        FirstDiv.attr('id', 'multiCollapseExample');
        FirstDiv.addClass('collapse');
        FirstDiv.addClass('multi-collapse');

        var SecondDiv = $('<div>');
        SecondDiv.addClass('card-body');
        SecondDiv.addClass('timeline-label');

        var UserType = $('.LastBlock').attr('id');
        if (UserType == "Company") {
            SecondDiv.attr('id', 'divContentCompany')
        }
        else {
            SecondDiv.attr('id', 'divContentCustomer')
        }
        SecondDiv.addClass('border-top');

        var h4section = $('<h4>');
        h4section.text('Change the data');
        SecondDiv.append(h4section);
        SecondDiv.append('<hr>');

        var formsection = $('<form>');
        formsection.attr('name', 'postform');
        formsection.attr('id', 'postform');
        formsection.addClass("form-horizontal");

        var divInlineInputs = $('<div>');
        divInlineInputs.addClass("row");

        var thirdDiv = $('<div>');
        thirdDiv.addClass('form-group');
        thirdDiv.addClass('col-sm-6');
        var labelsection = $('<label>');
        labelsection.addClass('h4');
        labelsection.text('Price');
        var inputSection = $('<input>');
        inputSection.attr('type', "number");
        inputSection.attr('min', 1);
        inputSection.attr('id', "txtPrice");
        inputSection.addClass('form-control');
        inputSection.prop('required', true);
        thirdDiv.append(labelsection);
        thirdDiv.append(inputSection);

        var fourthDiv = $('<div>');
        fourthDiv.addClass('form-group');
        fourthDiv.addClass('col-sm-6');
        var labelsection2 = $('<label>');
        labelsection2.addClass('h4');
        labelsection2.text('Date');

        var inputSection2 = $('<input>');
        inputSection2.attr('type', "date");
        inputSection2.attr('min', ValidationCurrentDate(true));
        inputSection2.attr('max', ValidationCurrentDate(false));
        inputSection2.attr('id', "txtDate");
        inputSection2.addClass('form-control');
        inputSection2.prop('required', true);

        var inputSection3 = $('<input>');
        inputSection3.attr('id', 'txtId');
        inputSection3.attr('type', 'hidden');

        fourthDiv.append(labelsection2);
        fourthDiv.append(inputSection2);
        fourthDiv.append(inputSection3);

        divInlineInputs.append(thirdDiv);
        divInlineInputs.append(fourthDiv);

        formsection.append(divInlineInputs);

        var submitButton = $('<button>');
        submitButton.attr('type', 'submit');
        submitButton.attr('id', 'sbmBtn');
        submitButton.prop('disabled', true);
        submitButton.addClass('btn btn-default');
        submitButton.text('Submit');

        var submitButton2 = $('<button>');
        submitButton2.attr('type', 'button');
        submitButton2.attr('id', 'closeBtn');
        submitButton2.addClass('btn btn-default');
        submitButton2.text('Close');

        formsection.append(submitButton);
        formsection.append(submitButton2);

        SecondDiv.append(formsection);
        FirstDiv.append(SecondDiv);
        $('#was').append(FirstDiv);
    });

function ValidationCurrentDate(StartDate) {
    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth() + 1;
    var day;
    var CurrentDay = d.getDate();
    if (CurrentDay < 10) {
        day = "0" + CurrentDay;
    }
    else
        day = CurrentDay;
    var date = year + "-" + month + "-" + day;
    var endDate = year + 1 + "-" + AddMonth(month + 1) + "-" + day;
    if (StartDate == true)
        return date;
    else
        return endDate;
};
function AddMonth(month) {
    var newMonth = '0';
    if (month > 12)
        month -= 12;
    else
        newMonth = month;
    if (month < 10)
        newMonth += month;
    return newMonth;
}