﻿var OrderHistoryStatus = {
    "Pending":1,
    "Changed":2,
    "Accepted":3,
    "Canceled":4,
    "New":5
};
var RequestStatus = {
    "New": 1,
    "Pending": 2,
    "Accepted": 3,
    "Canceled": 4,
    "Archived": 5,
    "All":6
};

