﻿using BussinessLogicLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BussinessLogicLayer.Models
{
    public class CompanyNotificationsDTO
    {
        public int? Id { get; set; }

        public string Price { get; set; }

        public DateTime ScheduleDate { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Message { get; set; }

        public string OfferCategory { get; set; }

        public bool IsShown { get; set; }

        public int OrderId { get; set; }

        public int? RequestId { get; set; }

        public static CompanyNotificationsDTO Init(OrderHistory orderHistory, string name, string message, string offerCategory)
        {
            return new CompanyNotificationsDTO
            {
                Price = orderHistory.Price,
                ScheduleDate = orderHistory.ScheduleDate,
                CreateDate = orderHistory.UpdateDate,
                //Status = orderHistory.Status,
                Name = name,
                Message = message,
                OfferCategory = offerCategory,
                IsShown = orderHistory.IsShown,
                Id = orderHistory.Id,
                OrderId = orderHistory.Order.Id,
                RequestId = orderHistory.Order.RequestId

            };
        }

        public static CompanyNotificationsDTO Init(Order order, string name, string message, string offerCategory)
        {
            return new CompanyNotificationsDTO
            {
                Price = order.Price,
                ScheduleDate = order.ExecutionDate,
                Name = name,
                Message = message,
                OfferCategory = offerCategory,
                IsShown = false,
                OrderId = order.Id

            };
        }

    }
}