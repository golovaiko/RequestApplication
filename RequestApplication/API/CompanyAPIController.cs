﻿using BussinessLogicLayer.Models;
using BussinessLogicLayer.Repositories;
using BussinessLogicLayer.Services;
using RequestApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace RequestApplication.API
{
    [System.Web.Http.Authorize]
    public class CompanyAPIController : BaseApiController
    {
        readonly IRequestRepository _requestRepository;
        readonly IOrderRepository _orderRepository;
        readonly ICompanyRepository _companyRepository;
        readonly ICustomerRepository _customerRepository;

        public CompanyAPIController(IRequestRepository requestRepository,
            IOrderRepository orderRepository,
            ICompanyRepository companyRepository,
            ICustomerRepository customerRepository
            )
        {
            _requestRepository = requestRepository;
            _orderRepository = orderRepository;
            _companyRepository = companyRepository;
            _customerRepository = customerRepository;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Authorize]
        public List<OrderListModel> GetOrders()
        {
           //UserId
            var orders = _orderRepository.GetAll().Where(order => order.CompanyId == UserId).ToList();
            var result = new List<OrderListModel>();
            foreach (var item in orders) { 
                result.Add(new OrderListModel()
                {
                    Company = _companyRepository.GetById(item.CompanyId),
                    Request = _requestRepository.GetById(item.RequestId),
                    Order = _orderRepository.GetById(item.Id)
                    
                });
                
            }
            foreach (var item in result)
            {
                item.Request.Customer = _customerRepository.GetById(item.Request.CustomerId);
            }
                return result;
        }

    }
}