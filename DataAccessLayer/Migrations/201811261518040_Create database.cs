namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Createdatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Offers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.CompanyOffers",
                c => new
                    {
                        OfferId = c.Int(nullable: false),
                        CompanyId = c.String(nullable: false, maxLength: 128),
                        OfferPrice = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.OfferId, t.CompanyId })
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: true)
                .ForeignKey("dbo.Offers", t => t.OfferId, cascadeDelete: true)
                .Index(t => t.OfferId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        ProfileImage = c.Binary(),
                        PhoneNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        MinPrice = c.Int(nullable: false),
                        MaxPrice = c.Int(nullable: false),
                        Deadline = c.DateTime(),
                        Date = c.DateTime(nullable: false),
                        Status = c.String(),
                        CustomerId = c.String(maxLength: 128),
                        OfferId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerId)
                .ForeignKey("dbo.Offers", t => t.OfferId, cascadeDelete: true)
                .Index(t => t.CustomerId)
                .Index(t => t.OfferId);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Gender = c.String(nullable: false),
                        BirthDate = c.DateTime(nullable: false),
                        ProfileImage = c.Binary(),
                        PhoneNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.Int(nullable: false),
                        Changed = c.String(nullable: false),
                        Price = c.String(nullable: false),
                        ScheduleDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        IsShown = c.Boolean(nullable: false),
                        UserId = c.String(),
                        Order_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.Order_Id)
                .Index(t => t.Order_Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequestId = c.Int(nullable: false),
                        CompanyId = c.String(),
                        Price = c.String(nullable: false),
                        ExecutionDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UsersFeedbacks",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Subject = c.Int(nullable: false),
                        Message = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderHistories", "Order_Id", "dbo.Orders");
            DropForeignKey("dbo.Requests", "OfferId", "dbo.Offers");
            DropForeignKey("dbo.Requests", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.CompanyOffers", "OfferId", "dbo.Offers");
            DropForeignKey("dbo.CompanyOffers", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Offers", "CategoryId", "dbo.Categories");
            DropIndex("dbo.OrderHistories", new[] { "Order_Id" });
            DropIndex("dbo.Requests", new[] { "OfferId" });
            DropIndex("dbo.Requests", new[] { "CustomerId" });
            DropIndex("dbo.CompanyOffers", new[] { "CompanyId" });
            DropIndex("dbo.CompanyOffers", new[] { "OfferId" });
            DropIndex("dbo.Offers", new[] { "CategoryId" });
            DropTable("dbo.UsersFeedbacks");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderHistories");
            DropTable("dbo.Customers");
            DropTable("dbo.Requests");
            DropTable("dbo.Companies");
            DropTable("dbo.CompanyOffers");
            DropTable("dbo.Offers");
            DropTable("dbo.Categories");
        }
    }
}
