﻿using BussinessLogicLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RequestApplication.Models
{
    public class RequestViewModel
    {
        public int RequestId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        public DateTime? Deadline { get; set; }
        public DateTime Date { get; set; }
        public int Status { get; set; }
        public string OfferName { get; set; }
        public string CategoryName { get; set; }

        public static RequestViewModel Init(Request request, Offer offer, Category category)
        {
            RequestViewModel rvm = new RequestViewModel() {
                RequestId = request.Id,
                Title = request.Title,
                Description = request.Description,
                MinPrice = request.MinPrice,
                MaxPrice = request.MaxPrice,
                Deadline = request.Deadline,
                Date = request.Date,
                Status = request.Status,
                OfferName = offer!=null? offer.Name:"",
                CategoryName = category!=null? category.Name: "" };
            return rvm;
        }

    }
}