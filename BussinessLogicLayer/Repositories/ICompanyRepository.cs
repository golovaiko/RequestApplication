﻿using BussinessLogicLayer.Core;
using BussinessLogicLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Repositories
{
    public interface ICompanyRepository : IRepository<Company , string>
    {
        IEnumerable<Company> GetCompaniesByOfferId(int? id);
        IEnumerable<Company> GetCompaniesFromOrderHistories(List<string> OrderHistoriesUserIds);

    }
}
