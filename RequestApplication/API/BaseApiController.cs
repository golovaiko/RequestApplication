﻿using System.Linq;
using System.Security.Claims;
using System.Web.Http;

namespace RequestApplication.API
{
    public class BaseApiController: ApiController
    {
        public string UserId
        {
            get
            {
                var identity = new ClaimsIdentity(User.Identity);
                var claim = identity.Claims.FirstOrDefault(x => x.Type == "userId");
                if (claim != null)
                {
                    return claim.Value;
                }

                return string.Empty;
            }
        }
    }
}