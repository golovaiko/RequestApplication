﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Services
{
    public enum StatusesForOrderHistory
    {
        Pending=1 ,
        Changed,
        Accepted,
        Canceled,
        New
    }
}
