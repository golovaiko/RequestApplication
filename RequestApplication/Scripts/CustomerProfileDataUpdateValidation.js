﻿$("#nameTextBox").change(function () {
    validateNameTextBox();
});
$("#nameTextBox").focusout(function () {
    validateNameTextBox();
});

$("#surnameTextBox").change(function () {
    validateSurnameTextBox();
});
$("#surnameTextBox").focusout(function () {
    validateSurnameTextBox();
});

$("#phoneNumberTextBox").change(function () {
    validateCustomerPhoneNumberTextBox();
});
$("#phoneNumberTextBox").focusout(function () {
    validateCustomerPhoneNumberTextBox();
});

function resetCustomerProfileUpdateValidationStyles() {
    $("#nameTextBox").removeClass('errorInput');
    $("#surnameTextBox").removeClass('errorInput');
    $("#phoneNumberTextBox").removeClass('errorInput');

    $("#invalidNameOrSurname").hide();
    $("#invalidPhoneNumber").hide();
}

function validateNameTextBox() {
    let textBox = $("#nameTextBox");
    if (textBox.val() === null || textBox.val().trim() === '') {
        textBox.addClass('errorInput');
        return false;
    }
    else {
        textBox.removeClass('errorInput');
        return true;
    }
}

function validateSurnameTextBox() {
    let textBox = $("#surnameTextBox");
    if (textBox.val() === null || textBox.val().trim() === '') {
        textBox.addClass('errorInput');
        return false;
    }
    else {
        textBox.removeClass('errorInput');
        return true;
    }
}

function validateCustomerPhoneNumberTextBox() {
    let phoneNumberTextBox = $("#phoneNumberTextBox");

    if (CheckIfAllDigits(phoneNumberTextBox.val()) && Number.parseInt(phoneNumberTextBox.val()) > 0) {
        phoneNumberTextBox.removeClass('errorInput');
        return true;
    }
    else {
        phoneNumberTextBox.addClass('errorInput');
        return false;
    }
}