﻿using BussinessLogicLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessLogicLayer.Models;

namespace DataAccessLayer.Repositories
{
    public class CompanyOfferRepository : Repository<CompanyOffer , int> , ICompanyOfferRepository
    {
        public CompanyOfferRepository (UnitOfWork unitOfWork) : base (unitOfWork)
        {

        }

        public bool IsProviding(int offerId, string companyId)
        {
            return UnitOfWork.Context.CompanyOffers.Any(cf => cf.CompanyId == companyId && cf.OfferId == offerId);
        }

        public bool IsProviding(int offerId, string companyId, out CompanyOffer recordId)
        {
            if (!UnitOfWork.Context.CompanyOffers.Any(cf => cf.CompanyId == companyId && cf.OfferId == offerId))
            {
                recordId = new CompanyOffer { OfferId = -1 };
                return false;
            }
            recordId = UnitOfWork.Context.CompanyOffers.First(cf => cf.CompanyId == companyId && cf.OfferId == offerId);
            return true;
        }

        public CompanyOffer GetNeededCompanyOffer(Company i,Request justMadeRequest)
        {
            return UnitOfWork.Context.CompanyOffers.Where(x => x.CompanyId == i.Id && x.OfferId == justMadeRequest.OfferId).Single();
        }
    }
}