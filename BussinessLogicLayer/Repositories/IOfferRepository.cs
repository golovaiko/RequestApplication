﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessLogicLayer.Core;
using BussinessLogicLayer.Models;

namespace BussinessLogicLayer.Repositories
{
    public interface IOfferRepository : IRepository<Offer, int>
    {

    }
}
