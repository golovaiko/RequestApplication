﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessLogicLayer.Models;
using BussinessLogicLayer.Repositories;
using System.Data.Entity;

namespace DataAccessLayer.Repositories
{
    public class OrderHistoryRepository : Repository<OrderHistory, int>, IOrderHistoryRepository
    {
        public OrderHistoryRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public IEnumerable<OrderHistory> FindWithOrder(int orderId)
        {
            return UnitOfWork.Context.OrderHistories.Include(x => x.Order).Where(x => x.Order.Id == orderId).ToList();
        }

        public IEnumerable<OrderHistory> GelAllWithOrder()
        {
            return UnitOfWork.Context.OrderHistories.Include(x => x.Order).ToList();
        }

        public IEnumerable<OrderHistory> GelAllWithOrder(Func<OrderHistory, bool> predicate)
        {

            return UnitOfWork.Context.OrderHistories.Include(x => x.Order).Where(predicate).ToList();
        }

        public List<OrderHistory> GetOrderHistories(List<int> requestIds)
        {
            return UnitOfWork.Context.OrderHistories
                .Include(x => x.Order)
                .Where(x => requestIds.Contains(x.Order.RequestId) && x.IsShown == false)
                .ToList();
        }

        public OrderHistory GetOrderHistoryWithOrder(int orderHistoryId)
        {
            return UnitOfWork.Context.OrderHistories.Include(x => x.Order).Where(x => x.Id == orderHistoryId).FirstOrDefault();
        }

        public OrderHistory GetLastWithOrder(int OrderId)
        {
            return UnitOfWork.Context.OrderHistories.Include(x => x.Order).Where(x => x.Order.Id == OrderId).ToList().LastOrDefault();

        }

        public List<OrderHistory> GetCompanyOrderHistories(string CurrentUserId)
        {
            return UnitOfWork.Context.OrderHistories.Include(x => x.Order)
                .Where(x => x.Order.CompanyId == CurrentUserId && x.UserId != CurrentUserId && x.IsShown == false)
                .ToList();
        }

        public void ChangeOrderHistoryActiveStatusByOrderId(int orderId)
        {
            UnitOfWork.Context.OrderHistories.Include(x => x.Order)
                .Where(x => x.Order.Id == orderId && x.IsShown == false)
                .ToList()
                .ForEach(y => y.IsShown = true);
        }
    }
}
