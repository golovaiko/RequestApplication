﻿using BussinessLogicLayer.Models;
using BussinessLogicLayer.Models.DTO;
using BussinessLogicLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Services
{
    public class NotificationsService : INotificationsService
    {
        readonly IOrderHistoryRepository _orderHistoryRepository;
        readonly IRequestRepository _requestRepository;
        readonly IOrderRepository _orderRepository;
        readonly ICustomerRepository _customerRepository;
        readonly ICompanyRepository _companyRepository;
        readonly IOrderHistoryService _orderHistoryService;


        public NotificationsService(IOrderHistoryRepository orderHistoryRepository, IRequestRepository requestRepository,
            IOrderRepository orderRepository, ICustomerRepository customerRepository, ICompanyRepository companyRepository,IOrderHistoryService orderHistoryService)
        {
            _orderHistoryRepository = orderHistoryRepository;
            _requestRepository = requestRepository;
            _orderRepository = orderRepository;
            _customerRepository = customerRepository;
            _companyRepository = companyRepository;
            _orderHistoryService = orderHistoryService;
        }

        public List<OrderHistory> GetCustomerOrderHistories(string CurrentUser)
        {
            List<OrderHistory> CustomerOrderHistories = new List<OrderHistory>() ;
            if (!string.IsNullOrEmpty(CurrentUser))
            {
                var requests = _requestRepository.GetByUserId(CurrentUser).ToList();
                var orderHistories = _orderHistoryRepository.GetOrderHistories(requests.Select(x => x.Id).ToList());
                var validCustomerIds = _customerRepository.GetValidCustomerIds(orderHistories.Select(x => x.UserId).ToList());
                CustomerOrderHistories = orderHistories.Where(x => !validCustomerIds.Contains(x.UserId)).ToList();
                return CustomerOrderHistories;
            }
            else
                return CustomerOrderHistories;
        }

        public List<OrderHistory> GetCompanyOrderHistories(string currentUserId)
        {
            return _orderHistoryRepository.GetCompanyOrderHistories(currentUserId);
        }

        public IEnumerable<Company> GetCompaniesFromOrderHistories(List<string> UserIds)
        {
            return _companyRepository.GetCompaniesFromOrderHistories(UserIds);
        }
        public List<CompanyNotificationsDTO> GetCompanysNotifications(string id)
        {
            var CompanyNotificationsDTOList = new List<CompanyNotificationsDTO>();
            var companyOrders = _orderRepository.GetOrdersByUserId(id);
            var requestsByCompanysOrders = _requestRepository.GetRequestsByOrdersIds(companyOrders.Select(x => x.RequestId)).ToList();
            var companysCustomers = requestsByCompanysOrders.Select(x => x.Customer).ToList();
            var companyRequestsIds = companyOrders.Select(x => x.RequestId).ToList();
            
            string CustomerName = "";
            string Message = "";
            string OfferCategory = "";
            int OrderHistoryCount = 0;
            foreach (var order in companyOrders)
            {
                OrderHistoryCount = order.OrderHistories.Count();
                if (OrderHistoryCount == 0)
                {
                    CustomerName = companysCustomers
                        .Where(x => companyRequestsIds.Contains(order.RequestId))
                        .Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                    Message = $"New order has been created by {CustomerName}";
                    OfferCategory = requestsByCompanysOrders
                        .Where(x => x.Id == order.RequestId)
                        .Select(x => x.Offer.Name + "/" + x.Offer.Category.Name)
                        .FirstOrDefault();
                    CompanyNotificationsDTOList.Add(CompanyNotificationsDTO.Init(order, CustomerName, Message, OfferCategory));
                }
            }
            var CompanyOrderHistories = _orderHistoryRepository.GetCompanyOrderHistories(id);
            foreach (var orderHisory in CompanyOrderHistories)
            {
                CustomerName = companysCustomers.Where(x => x.Id == orderHisory.UserId).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                OfferCategory = requestsByCompanysOrders.Where(x => x.Id == orderHisory.Order.RequestId).Select(x => x.Offer.Name + "/" + x.Offer.Category.Name).FirstOrDefault();
                Message = GetCompanyMessage(orderHisory.Status,CustomerName);
                CompanyNotificationsDTOList.Add(CompanyNotificationsDTO.Init(orderHisory, CustomerName, Message, OfferCategory));
            }
            return CompanyNotificationsDTOList;
        }

        public List<CustomerNotificationsDTO> GetCustomersNotifications(string userId)
        {
            var CustomerNotifications = new List<CustomerNotificationsDTO>();
            if (!string.IsNullOrEmpty(userId))
            {
                var CustomerOrderHistories = GetCustomerOrderHistories(userId);
                var OrderHistoryCompany = _companyRepository.GetCompaniesFromOrderHistories(CustomerOrderHistories.Select(x => x.UserId).ToList());
                foreach (var orderHist in CustomerOrderHistories)
                {
                    string CompanyName = OrderHistoryCompany.Where(x => x.Id == orderHist.UserId).Select(x => x.Name).FirstOrDefault();
                    string Message = GetCustomerMessage(orderHist.Status);
                    CustomerNotifications.Add(CustomerNotificationsDTO.Init(orderHist, CompanyName, Message));
                }
            }
            return CustomerNotifications;
        }

        public void ChangeNotificationStatus(int id)
        {
            OrderHistory orderHistory = _orderHistoryRepository.GetById(id);
            orderHistory.IsShown = true;
            _orderHistoryRepository.Update(orderHistory);
            _orderHistoryRepository.SaveChanges();
        }

        public void DeleteNotificationsByOrderId(int orderId)
        {
            _orderHistoryRepository.ChangeOrderHistoryActiveStatusByOrderId(orderId);
            _orderHistoryRepository.SaveChanges();
        }

        public CurrentChatOrderHistoryData GetCurrentChatOrderHistory(int OrderId)
        {
            var LastOrderHistory = _orderHistoryRepository.GetLastWithOrder(OrderId);
            CurrentChatOrderHistoryData currentChatOrderHistoryData = CurrentChatOrderHistoryData.Init(LastOrderHistory, _orderHistoryService.AdditionInfoAboutUser(LastOrderHistory.UserId));
            return currentChatOrderHistoryData;
        }

        private string GetCustomerMessage(int status)
        {
            string message = "";

            switch (status)
            {
                case 1:
                    message = "Your request has been approved.";
                    break;
                case 2:
                    message = "Your request has been changed.";
                    break;
                case 4:
                    message = "Your request has been canceled.";
                    break;
                case 3:
                    message = "Your request has been accepted.";
                    break;
            }
            return message;
        }

        private string GetCompanyMessage(int status, string customer)
        {
            string message = "";

            switch (status)
            {
                case 0:
                    message = "New order has been creted by " + customer;
                    break;
                case 2:
                    message = customer + " has changed the order data.New data bellow:";
                    break;
                case 3:
                    message = customer + " has accepted your requirements...";
                    break;
                case 4:
                    message = customer + " has canceled the order...";
                    break;
            }
            return message;

        }

    }
}
