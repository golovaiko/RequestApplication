﻿var parag = $(".description-text");
parag.each(function (i) {
    var paragText = $.trim($(this).text());
    if (paragText.length > 250) {
        paragText = paragText.substring(0, 250);
        paragText = paragText + "..."
        $(this).text(paragText);
        $(this).append("<a href='#' class='more-link'> More&raquo;</a>");
    }
})