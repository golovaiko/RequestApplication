﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessLogicLayer.Core;
using BussinessLogicLayer.Models;

namespace BussinessLogicLayer.Repositories
{
    public interface ICompanyOfferRepository : IRepository<CompanyOffer, int>
    {
        bool IsProviding(int offerId, string companyId);
        bool IsProviding(int offerId, string companyId, out CompanyOffer recordId);
        CompanyOffer GetNeededCompanyOffer(Company i, Request justMadeRequest);
    }
}
