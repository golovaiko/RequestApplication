﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessLogicLayer.Models;
using BussinessLogicLayer.Repositories;
using System.Data.Entity;
using BussinessLogicLayer.Services.Statuses;

namespace DataAccessLayer.Repositories
{
    public class RequestRepository : Repository<Request, int>, IRequestRepository
    {
        public RequestRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
        public IEnumerable<Request> GetByUserId(string userId)
        {

            if (userId != null)
            {
                var item = UnitOfWork.Context.Requests.Where(x => x.CustomerId == userId).ToList();
                foreach (var rt in item)
                {

                    rt.Offer = UnitOfWork.Context.Offers.Where(off => off.Id == rt.OfferId).Single();

                    rt.Offer.Category = UnitOfWork.Context.Categories.Where(cat => cat.Id == rt.Offer.CategoryId).Single();


                }
                return item;
            }
            else
            {
                return GetAll();
            }
        }

        public Offer GetOffer(int id)
        {
            return UnitOfWork.Context.Requests.Where(r => r.Id == id).Join(UnitOfWork.Context.Offers, r => r.Offer, o => o, (r, o) => o).
                AsEnumerable().ToList()[0];
        }


        public IEnumerable<Request> GetAllWithCustomer(Func<Request, bool> predicate)
        {

            return UnitOfWork.Context.Requests.Include(x => x.Customer).Where(predicate).ToList();
        }

        public IEnumerable<int> GetRequestsStatusByUserId(string UserId)
        {
            return UnitOfWork.Context.Requests.Where(x => x.CustomerId == UserId)
                .Select(x => x.Status).ToList();
        }

        public Request GetLastRequest()
        {
            return UnitOfWork.Context.Requests.OrderByDescending(x => x.Id).Take(1).Single();
        }

        public List<Request> GetRequestByIdAndStatus(string userId, int status)
        {
            if (status == (int)StatusesForRequest.All)
            {
                return UnitOfWork.Context.Requests
                    .Include(x => x.Offer.Category)
                    .Where(x => x.CustomerId == userId)
                    .ToList();
            }
            return UnitOfWork.Context.Requests.Include(x => x.Offer.Category)
                .Where(x => x.CustomerId == userId && x.Status == status)
                .ToList();
        }

        public List<Request> GetRequestsByOrdersIds(IEnumerable<int> OrdersIds)
        {
            return UnitOfWork.Context.Requests.Include(x => x.Offer.Category)
                .Include(x => x.Customer)
                .Where(x => OrdersIds.Contains(x.Id))
                .ToList();
        }

    }
}
