﻿using System.Collections.Generic;
using System.Linq;
using BussinessLogicLayer.Models;
using BussinessLogicLayer.Repositories;
using BussinessLogicLayer.Models.DTO;
using AutoMapper;
using BussinessLogicLayer.Core;
using BussinessLogicLayer.Services.Statuses;
using System;

namespace BussinessLogicLayer.Services
{
    public class OrderHistoryService : IOrderHistoryService
    {
        IRequestRepository _requestRepository;
        IOrderHistoryRepository _orderHistoryRepository;
        IOrderRepository _orderRepository;
        ICustomerRepository _customerRepository;
        ICompanyRepository _companyRepository;
        //IUnitOfWork _unitOfWork;
        public OrderHistoryService(IRequestRepository requestRepository, IOrderHistoryRepository orderHistoryRepository, IOrderRepository orderRepository, ICustomerRepository customerRepository, ICompanyRepository companyRepository/*, IUnitOfWork unitOfWork*/)
        {
            _requestRepository = requestRepository;
            _orderHistoryRepository = orderHistoryRepository;
            _orderRepository = orderRepository;
            _customerRepository = customerRepository;
            _companyRepository = companyRepository;
            //_unitOfWork = unitOfWork;
        }
        
        public Request GetRequest(int id)
        {
           return _requestRepository.GetById(id); ;
        }

        public IEnumerable<OrderHistory> Find(int orderId)
        {
           IEnumerable<OrderHistory> orderHistory=_orderHistoryRepository.FindWithOrder(orderId).AsEnumerable();
           return orderHistory;
        }
        
        public Order GetOrder(int id)
        {
            return _orderRepository.GetById(id);
        }

        public Customer GetCustomer(string id)
        {
            return _customerRepository.GetById(id);
        }

        public Company GetCompany(string id)
        {
            return _companyRepository.GetById(id);
        }

        public OrderHistory GetOrderHistoryById(int id)
        {
            return _orderHistoryRepository.GetOrderHistoryWithOrder(id);
        }

        public InformationAboutUser AdditionInfoAboutUser(string id)
        {
            var userInfo = new InformationAboutUser();
            var customer = _customerRepository.GetById(id);
            if (customer == null)
            {
                userInfo.NameOfUser = _companyRepository.GetById(id).Name;
                userInfo.TypeOfUser = "Company";
            }
            else
            {
                userInfo.NameOfUser = customer.FirstName;
                userInfo.TypeOfUser = "Customer";
            }
            return userInfo;
        }

        public void AddOnlyWithAC(OrderHistoryDTO orderHistoryDTO, int newStatus)
        {
            
           OrderHistory orderHistory = MapDTOToItem(orderHistoryDTO, newStatus);

            var request = _requestRepository.GetById(orderHistory.Order.RequestId);
            request.Status = (int)StatusesForRequest.Pending;

            OrderHistory lastElement = null;
            var orderHistoryStatus = (StatusesForOrderHistory)orderHistory.Status;
            switch (orderHistoryStatus)
            {
                case StatusesForOrderHistory.Changed:
                    lastElement = FindLastByOrderId(orderId: orderHistory.Order.Id);
                    break;
                case StatusesForOrderHistory.Pending:
                    orderHistory = AssignPriceAndScheduleDate(orderHistory);
                    break;
                default:
                    _requestRepository.Update(request);
                    lastElement = FindLastByOrderId(orderId: orderHistory.Order.Id, isPendingOrAccepted: true);
                    break;
            }

            orderHistory.Changed = AssignChanged(orderHistory, lastElement);

            if (orderHistoryStatus == StatusesForOrderHistory.Accepted) ChangeDataForStatusAccept(orderHistory, request); 

            var order = orderHistory.Order;
            orderHistory.Order = null;

            order = _orderRepository.GetByIdWithOrderHistory(order.Id);
            order.OrderHistories.Add(orderHistory);

            _requestRepository.SaveChanges();
            _orderRepository.SaveChanges();
        }

        public int CheckIfAgreed(int idRequest)
        {
            var AllIdOrders = _orderRepository.Find(idRequest).ToList();
            //if is not Order with status Accepted>>idWithStatusAccepted=-1, if status of request is Archived>>idWithStatusAccepted=-2
            int idWithStatusAccepted =-1;
            var LastOrderHistories = _orderRepository.GetLastOrderHistoryByOrder(idRequest);
            foreach (var orderHistory in LastOrderHistories)
            {
                if (orderHistory != null && orderHistory.Status == (int)StatusesForOrderHistory.Accepted)
                {
                    idWithStatusAccepted = orderHistory.Order.Id;
                }
            }
            idWithStatusAccepted = (_requestRepository.GetById(idRequest).Status == (int)StatusesForRequest.Archived) ? -2 : idWithStatusAccepted;
            //var getOfRequestStatus = _requestRepository.GetById(idRequest).Status?.ToString();          
            return idWithStatusAccepted;
        }

        //OH-orderHistory
        public string AssignChanged(OrderHistory currentOrderHistory, OrderHistory lastElement)
        {
            if (lastElement == null) return "All";
            if (lastElement.Price != currentOrderHistory.Price && lastElement.ScheduleDate == currentOrderHistory.ScheduleDate) return "Price";
            if (lastElement.Price == currentOrderHistory.Price && lastElement.ScheduleDate != currentOrderHistory.ScheduleDate) return "DateTime";
            return "All";
        }

        public Request GetCurrentRequestForOrderHistory(OrderHistoryDTO item)
        {
            return _requestRepository.GetById(item.Order.RequestId);
        }
        private OrderHistory MapDTOToItem(OrderHistoryDTO orderHistoryDTO, int newStatus)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<OrderHistoryDTO, OrderHistory>()
                    .ForMember("Status", opt => opt.MapFrom(c => newStatus))
                    .ForMember("IsShown", opt => opt.MapFrom(c => false)));
            OrderHistory orderHistory = Mapper.Map<OrderHistoryDTO, OrderHistory>(orderHistoryDTO);
            Mapper.Reset();

            return orderHistory;
        }

        private OrderHistory AssignPriceAndScheduleDate(OrderHistory orderHistory)
        {
            var orderItem = _orderRepository.GetById(orderHistory.Order.Id);
            orderHistory.Price = orderItem.Price;
            orderHistory.ScheduleDate = orderItem.ExecutionDate;
            return orderHistory;
        }

        private OrderHistory FindLastByOrderId(int orderId, bool isPendingOrAccepted = false)
        {
            var query = _orderHistoryRepository.FindWithOrder(orderId);
            if (isPendingOrAccepted)
            {
                query = query.Where(x => x.Status == (int)StatusesForOrderHistory.Pending || x.Status == (int)StatusesForOrderHistory.Accepted);
            }
            return query.LastOrDefault();
        }
        private void ChangeDataForStatusAccept(OrderHistory orderHistory, Request request)
        {
            orderHistory.Order.Price = orderHistory.Price;
            orderHistory.Order.ExecutionDate = orderHistory.ScheduleDate;
            request.Deadline = orderHistory.ScheduleDate;
            request.Status = (int)StatusesForRequest.Accepted;
            _requestRepository.Update(request);
            _orderRepository.Update(orderHistory.Order);
            _orderRepository.SaveChanges();
        }
    }
}
