﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessLogicLayer.Models;
using BussinessLogicLayer.Repositories;

namespace DataAccessLayer.Repositories
{
    public class OrderRepository : Repository<Order , int> , IOrderRepository
    {
        public OrderRepository (UnitOfWork unitOfWork) : base (unitOfWork)
        {
            
        }

        public List<Order> GetByRequestId(int id)
        {
            return GetAll(r => r.RequestId == id).ToList();
        }

        public List<Request> GetByCompanyId(string companyId)
        {
            var ids = new List<int>();
            var reqList= UnitOfWork.Context.Orders.Where(r => r.CompanyId == companyId)
                .Join(UnitOfWork.Context.Requests, r => r.RequestId, o => o.Id, (r, o) => o)
                .AsEnumerable().ToList();
            
            return reqList;
        }

        public Order GetByIdWithOrderHistory(int id)
        {
            return UnitOfWork.Context.Orders.Include("OrderHistories").FirstOrDefault(x => x.Id == id);
        }

        public List<Order> Find(int requestId)
        {
            return UnitOfWork.Context.Orders.Where(x => x.RequestId == requestId).ToList();
        }
        
        public List<Order> GetAllWithOrderHistory()
        {
            return UnitOfWork.Context.Orders.Include("OrderHistories").ToList();
        }

        public List<Order> GetOrdersByUserId(string CurrentUserId)
        {
            return UnitOfWork.Context.Orders.Include("OrderHistories").Where(x => x.CompanyId == CurrentUserId).ToList();
        }
        public List<OrderHistory> GetLastOrderHistoryByOrder(int idRequest)
        {
            var result = UnitOfWork.Context.Orders.Include("OrderHistories").ToList();          
            return result.Where(x => x.RequestId == idRequest).Select(x => x.OrderHistories.LastOrDefault()).ToList(); ;
        }
    }
}
