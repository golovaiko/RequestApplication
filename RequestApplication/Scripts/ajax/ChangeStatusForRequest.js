﻿var token = sessionStorage.getItem('tokenKey');
function changeToolbarStatus(newStatus, currentStatus, requestId, deadline) {
    if (newStatus == currentStatus) {
        $.ambiance({
            message: "The request already have this status ",
            title: "Default",
            type: "default"
        })
    }
    else if (newStatus == RequestStatus.Archived) {
        if ($('div#ambiance-notification').html() == '') {
            var buttonYes = $(window.document.createElement('button'))
       .attr("onclick", 'ChangeArchived(true,"' + newStatus + '",' + requestId + ')')
       .html("Yes");
            var buttonNot = $(window.document.createElement('button'))
                           .attr("onclick", 'ChangeArchived(false,"'+newStatus+'",'+requestId+')')
                           .html("No");
            var formForArchived = $(window.document.createElement('p')).text("Are you sure?  ").append(buttonYes).append(buttonNot);
            $.ambiance({
                message: formForArchived,
                permanent: true,
                fade: true,
            })
        }
    }
    else if (newStatus != currentStatus) {
        $("div#allList").empty();       
        sendChangesRequest(newStatus, requestId);
            $("div#tabs-company").remove();
            $("div#tabsOrderHistory").remove();      
    }
}

function sendChangesRequest(newStatus, requestId)
{
    $.ajax({
        type: "POST",
        url: "/api/RequestAPI/ChangeStatus/?status=" + newStatus + "&requestId=" + requestId,
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        dataType: "json",
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function () {
            $.ambiance({
                message: "Status successfully changed!",
                title: "Success",
                type: "success"
            })
            $("div#allList").empty();
            GetRequestsForCurrentUserByStatus(6);
        }, error: function () {
            $.ambiance({
                message: "Messages are not available, try again later",
                title: "Error!",
                type: "error"
            })
        }
    });
}
function ChangeArchived(response, newStatus, requestId) {

    if (response == true) {
        $("div#ambiance-notification").empty();
        sendChangesRequest(newStatus, requestId);
        //$("div#allList").empty();
        //GetRequestsForCurrentUserByStatus("All");
    }
    else {
        $("div#ambiance-notification").empty();
    }

}
