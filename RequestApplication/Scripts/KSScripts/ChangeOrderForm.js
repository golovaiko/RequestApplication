﻿
$('body').on('click', '#btn1', function () {
    var ChatOrderId = $("input#ChangingFormOrderId").val();
    var ChatRequestId = $('#btnMessages' + ChatOrderId).attr('data-request-id');

    $.ajax({
        url: '/api/NotificationsAPI/GetCurrentOrderHistory?OrderId=' + ChatOrderId,
        type: 'GET',
        dataType: 'json',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (result) {
            $('button#btn1').prop("disabled", true);
            $('button#ButtonCancel').prop("disabled", true);
            $('button#ButtonAccept').prop("disabled", true);
            $("#txtPrice").val(result.Price);
            $("#txtDate").val(GetData(result.ScheduleDate));
            $("#txtId").val(result.Id);
            $('#closeBtn').attr('data-toggle', 'collapse');
            $('#closeBtn').attr('href', '#multiCollapseExample');
            $('#closeBtn').attr('aria-expanded', 'true');
            $('#closeBtn').attr('role', 'button');
            $('#closeBtn').attr('aria-controls', 'multiCollapseExample');

            $('#txtPrice').change(function () {
                $('button#sbmBtn').prop("disabled", false);
            });

            $('#txtDate').change(function () {
                $('button#sbmBtn').prop("disabled", false);
            });


            $('#closeBtn').click(function () {
                $('button#btn1').prop("disabled", false);
                $('button#ButtonCancel').prop("disabled", false);
                $('button#ButtonAccept').prop("disabled", false);
            }
            );
        },
        error: function () {
            //alert('Error!');
        }
    });
    $("#postform").submit(function (e) {
        e.preventDefault();
        
        var apiurl = "/api/OrderHistoryAPI/ChangeRequest";
        var data = {
            Price: $("#txtPrice").val().trim(),
            ScheduleDate: $("#txtDate").val().trim(),
            Id: $("#txtId").val().trim(),
        }
        $.ajax({
            url: apiurl,
            type: 'POST',
            dataType: 'json',
            async: false,
            data: data,
            headers: {
                "Accept": "application/json",
                "Authorization": "Bearer " + token
            },
            success: function () {
                $("#postform").trigger('reset');
                $('#sbmBtn').prop('disabled', true);
            },
            error: function () {
            }
            
        });
        ShowMessages(ChatOrderId, ChatRequestId)
    });
});


function GetData(Data) {
    var d = new Date(Data);
    var year = d.getFullYear();
    var month = AddZeroToValue(d.getMonth() + 1);
    var currentDay = d.getDate();
    var day = AddZeroToValue(currentDay);
    var date = year + "-" + month + "-" + day;
    return date;
}

function AddZeroToValue(data) {
    if (data < 10)
        return "0" + data;
    else
        return data;
}

