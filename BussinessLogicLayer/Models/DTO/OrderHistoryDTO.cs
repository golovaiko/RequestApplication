﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Models.DTO
{
    public class OrderHistoryDTO
    {
        public int OrderHistoryId { get; set; }
        [Required]
        [Range(0, 5)]
        public int Status { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public string Price { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime ScheduleDate { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime UpdateDate { get; set; }

        [Required]
        public Order Order { get; set; }

        public string UserId { get; set; }

        public string TypeOfUser { get; set; }

        public string NameUser { get; set; }

        public string Changed { get; set; }

        public static OrderHistoryDTO Init(OrderHistory orderHistory, InformationAboutUser informationAboutUser)
        {
            return new OrderHistoryDTO()
            {
                OrderHistoryId = orderHistory.Id,
                Status = orderHistory.Status,
                Price = orderHistory.Price,
                ScheduleDate = orderHistory.ScheduleDate,
                UpdateDate = orderHistory.UpdateDate,
                Order = orderHistory.Order,
                UserId = orderHistory.UserId,
                Changed = orderHistory.Changed,
                TypeOfUser = informationAboutUser.TypeOfUser,
                NameUser = informationAboutUser.NameOfUser,
            };

        }
    }
}