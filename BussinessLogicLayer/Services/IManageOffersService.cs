﻿using BussinessLogicLayer.Models.DTO;

namespace BussinessLogicLayer.Services
{
    public interface IManageOffersService
    {
        OfferCollectionDTO GetProvidedOffers(string companyId, int categoryId);
        void SaveProvidedOffers(OfferCollectionDTO offerCollectionDTO);
    }
}
