﻿using BussinessLogicLayer.Models;
using BussinessLogicLayer.Models.DTO;
using BussinessLogicLayer.Repositories;
using BussinessLogicLayer.Services.Statuses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Services
{
    public class RequestCreationService : IRequestCreationService
    {
        readonly IRequestRepository _requestRepository;

        public RequestCreationService(IRequestRepository requestRepository)
        {
            _requestRepository = requestRepository;
        }


        public void CreateNewRequest(NewRequestDTO newRequest)
        {
            Request request = new Request()
            {
                Title = newRequest.Title,
                Date = DateTime.Now,
                MinPrice = newRequest.MinPrice,
                MaxPrice = newRequest.MaxPrice,
                OfferId = newRequest.OfferId,
                Deadline = newRequest.Deadline,
                CustomerId = newRequest.CustomerId,
                Description = newRequest.Description,
                Status = (int)StatusesForRequest.New
            };

            _requestRepository.Create(request);
            _requestRepository.SaveChanges();
        }
    }
}
