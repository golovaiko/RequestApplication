﻿var checkCompanyList = 0;

var token = sessionStorage.getItem('tokenKey');

function getCompanyListById(requestId) {
    $("#tabs-company").detach();
    $("#tabsOrderHistory").remove();
    $('.StatusCountValue').show();
    $("#loading").show(1000);

    $.get({
        url: "/API/RequestAPI/GetOrders?requestId=" + requestId,
        dataType: "json",
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (result) {
            var rowAdd = $('#addRow');
            var companyHead = createFormCompanyDomElement();
            rowAdd.append(initFormCompanyDomElement(companyHead.formHead));

            var requestList = $('#comList');
            var headOfCompany =
                '<div class="container">' +
                '<div class="orders-heading">' +
                '<h5><b>Company List</b></h5>' +
                '</div>' +
                '</div>';
            requestList.append(headOfCompany);
            $(result).each(function (i, item) {
                
                var rootCompanyElement = createCompanyDomElement(item);
                requestList.append(initCompanyDomElement(rootCompanyElement.label));
                $("#loading").hide();
            });
            if (checkCompanyList == requestId) {

                $("#tabs-company").detach();
                $('.item.request').css('background-color', 'white');
                checkCompanyList = 0;
                chek = 0;
            }
            else {
                checkCompanyList = requestId;
                $('.item.request').css('background-color', '');
                $('.item.request.' + requestId).css('background-color', '#F3F3F3');
            }
        },
        
        error: function (error) { console.log(error); }
    }
        
    );
};

function createFormCompanyDomElement() {
    return {
        formHead: {
            className: 'col requests-container',
            id: 'tabs-company',
            sens: {
                className: 'items',
                id: 'requestList',
                container: {
                    className: 'container',
                    id: 'comList',
                }
            }
        }
    }
}

function initFormCompanyDomElement(formHead) {
    var divForm = $('<div>');
    divForm.addClass(formHead.className);
    divForm.attr('id', formHead.id);

    var divItems = $('<div>');
    divItems.addClass(formHead.sens.className);
    divItems.attr('id', formHead.sens.id);
    divForm.append(divItems);

    var divCont = $('<div>');
    divCont.addClass(formHead.sens.container.className);
    divCont.attr('id', formHead.sens.container.id);
    divItems.append(divCont);


    return divForm;
}

function createCompanyDomElement(item) {
    return {
        label: {
            className: 'retew',
            container: {
                className: 'container',
                item: {
                    className: 'item order ' + item.Id,
                    article: {
                        className: 'item-info container',
                        heading: {
                            className: 'item-info-heading',
                            title: {
                                className: 'item-title',
                                text: 'Company',
                            },
                            date: {
                                className: 'item-date ml-auto',
                                textDate: {
                                    className: 'text-muted',
                                    //exdate: item.ExecutionDate,
                                },
                            },
                        },
                        rating: {
                            className: 'item-rating',
                            //rate: '☆☆☆☆☆',
                        },
                        description: {
                            className: 'order-description',
                            text: 'Ready to work on your request.',
                        },
                        conditions: {
                            className: 'item-conditions',
                            price: {
                                className: 'item-price text-muted',
                                money: {
                                    className: 'fas fa-money-bill',
                                    text: item.Price,
                                }
                            },
                            deadline: {
                                className: 'order-deadline text-muted',
                                calendar: {
                                    className: 'far fa-calendar-alt',
                                    text: new Date(item.ExecutionDate).toUTCString().replace('GMT', ''),
                                }
                            }
                        },
                    },
                    buttonGoToHistory: {
                        className: 'menu-right-button',
                        goToCompanyClick: {
                            className: 'fas fa-angle-right createChat',
                            id: 'btnMessages' + item.Id,
                            dataOrderId: item.Id,
                            dataRequestId: item.RequestId,
                            link: 'getOrderHistories(' + 'btnMessages' + item.Id + ')',
                        }
                    }
                }
            }
        }
    }
}


function initCompanyDomElement(label) {
    var divLabel = $('<div>');
    divLabel.addClass(label.className);
    var divContainer = $('<div>');
    divContainer.addClass(label.container.className);
    divLabel.append(divContainer);

    var divItem = $('<div>');
    divItem.addClass(label.container.item.className);
    divContainer.append(divItem);

    var divArticle = $('<div>');
    divArticle.addClass(label.container.item.article.className);
    divItem.append(divArticle);

    var divHeading = $('<div>');
    divHeading.addClass(label.container.item.article.heading.className);
    var divTitle = $('<div>');
    divTitle.addClass(label.container.item.article.heading.title.className);
    divTitle.text(label.container.item.article.heading.title.text);
    divHeading.append(divTitle);
    divArticle.append(divHeading);

    var divDate = $('<div>');
    divDate.addClass(label.container.item.article.heading.date.className);
    var divTextDate = $('<div>');
    divTextDate.addClass(label.container.item.article.heading.date.textDate.className);
    divTextDate.text(label.container.item.article.heading.date.textDate.exdate);
    divDate.append(divTextDate);
    divHeading.append(divDate);

    var divRating = $('<div>');
    divRating.addClass(label.container.item.article.rating.className);
    var spanRate = $('<span>');
    spanRate.text(label.container.item.article.rating.rate);
    divRating.append(spanRate);
    divArticle.append(divRating);

    var divDescription = $('<div>');
    divDescription.addClass(label.container.item.article.description);
    var pElemDesc = $('<p>');
    pElemDesc.text(label.container.item.article.description.text);
    divArticle.append(pElemDesc);
    divArticle.append(divDescription);

    var divCondition = $('<div>');
    divCondition.addClass(label.container.item.article.conditions.className);
    var divPrice = $('<div>');
    divPrice.addClass(label.container.item.article.conditions.price.className);
    var iElemMoney = $('<i>');
    iElemMoney.addClass(label.container.item.article.conditions.price.money.className);
    var spanElemMoney = $('<span>');
    spanElemMoney.text(label.container.item.article.conditions.price.money.text);
    divPrice.append(iElemMoney);
    divPrice.append(spanElemMoney);
    divCondition.append(divPrice);
    divArticle.append(divCondition);

    var divDeadLine = $('<div>');
    divDeadLine.addClass(label.container.item.article.conditions.deadline.className);
    var iElemCalendar = $('<i>');
    iElemCalendar.addClass(label.container.item.article.conditions.deadline.calendar.className);
    var spanElemCalendar = $('<span>');
    spanElemCalendar.text(label.container.item.article.conditions.deadline.calendar.text);
    divDeadLine.append(iElemCalendar);
    divDeadLine.append(spanElemCalendar);
    divCondition.append(divDeadLine);

    var divGotoHistory = $('<div>');
    divGotoHistory.addClass(label.container.item.buttonGoToHistory.className);
    var iElemButton = $('<i>');
    iElemButton.addClass(label.container.item.buttonGoToHistory.goToCompanyClick.className);
    iElemButton.attr('id', label.container.item.buttonGoToHistory.goToCompanyClick.id);
    iElemButton.attr('data-order-id', label.container.item.buttonGoToHistory.goToCompanyClick.dataOrderId);
    iElemButton.attr('data-request-id', label.container.item.buttonGoToHistory.goToCompanyClick.dataRequestId);
    iElemButton.attr('onclick', label.container.item.buttonGoToHistory.goToCompanyClick.link);
    divGotoHistory.append(iElemButton);
    divItem.append(divGotoHistory);

    return divLabel;
}


GetRequestsForCurrentUserByStatus(6);

var loginData = {
    grant_type: 'password',
    username: 'qwerty1@ukr.net',
    password: 'Qwerty!23'
};

function onLogin() {
    $.ajax({
        type: 'POST',
        url: '/token',
        data: loginData
    }).success(function (data) {
        console.log(data);
        // save to repository sessionStorage token acces
        sessionStorage.setItem('tokenKey', data.access_token);
        $('.form-login').submit();
    }).fail(function (data) {
        console.log(data);
    });

    return false;
}

function GetRequestsForCurrentUserByStatus(status) {
    $.ajax({
        url: "/API/RequestAPI/GetRequests?&status=" + status,
        type: 'GET',
        async: false,
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (result) {
            var countLenght = $("h5#TextMyRequest");
            countLenght.empty();
            countLenght.append("My request <span class='badge badge-light' id='circle'>" + result.length + "</span>");

            $(result).each(function (i, item) {
               
                var requestList = $('#allList');

                var rootElement = createRequestDomElement(item);
                var htmlArticle = initRequestDomElement(rootElement.article);

                requestList.append(htmlArticle);
                $("#loading").hide();

            })
        },
        
        error: function (error) { console.log(error); }
    })
}

function createRequestDomElement(item) {

    return {
        article: {
            className: 'item request' + ' ' + item.Id,
            note: {
                className: 'IdOfRequest',
                type: 'hidden',
                value: item.Id,
            },
            container: {
                className: 'item-info container',
                heading: {
                    className: 'item-info-heading',
                    title: {
                        className: 'item-title',
                        text: item.Title,
                    },
                    category: {
                        className: 'item-offer text-muted',
                        categ: {
                            className: 'text-muted',
                            text: item.Offer.Category.Name + '/' + item.Offer.Name,
                        },

                    },
                    date: {
                        className: 'item-date ml-auto text-muted',
                        text: new Date(item.Date).toUTCString().replace('GMT', ''),
                    },
                },
                conditions: {
                    className: 'item-conditions',
                    commonPrice: {
                        className: 'item-price-scope text-muted',
                        price: {
                            className: 'fas fa-money-bill',
                            text: item.MinPrice + '-' + item.MaxPrice,
                        },
                    },
                    deadline: {
                        className: 'item-deadline text-muted',
                        calendar: {
                            className: 'far fa-calendar-alt',
                            text: new Date(item.Deadline).toUTCString().replace('GMT', ''),
                        }
                    }
                },
                description: {
                    className: 'item-description',
                    text: {
                        className: 'description-text',
                        text: item.Description,
                    }
                },
                toolbar: {
                    className: 'item-toolbar',
                    referenceOpen: {
                        className: "downButtonRequest",
                        link: 'getCompanyListById(' + item.Id + ')  ',
                        open: {
                            className: 'far fa-envelope-open ',
                        },
                    },
                    referenceCancel: {
                        className: "downButtonRequest",
                        link: 'changeToolbarStatus(4, ' + '"' + item.Status + '"' + ', ' + item.Id + ', "' + item.Deadline + '")',
                        cancel: {
                            className: 'fas fa-times-circle',
                        },
                    },
                    referenceDelete: {
                        className: "downButtonRequest",
                        link: 'changeToolbarStatus(5, ' + '"' + item.Status + '"' + ', ' + item.Id + ', "' + item.Deadline + '") ',
                        delete: {
                            className: 'fas fa-trash-alt',
                        },
                    },
                    referencePending: {
                        className: "downButtonRequest",
                        link: 'changeToolbarStatus(2, ' + '"' + item.Status + '"' + ', ' + item.Id + ', "' + item.Deadline + '")',
                        pending: {
                            className: 'fas fa-caret-square-right ',
                        },
                    },
                    referenceStatus: {
                        link: "#",
                        status: {
                            className: 'badge status-badge ml-auto ' + item.Id,
                            text: item.Status,
                        },
                    },
                }
            },
            buttonGoCompany: {
                className: 'requestClick menu-right-button',
                goToCompanyClick: {
                    className: 'fas fa-angle-right',
                    link: 'getCompanyListById(' + item.Id + ')',
                }
            }
        }
    }
}

function initRequestDomElement(article) {
    var divArticle = $('<div>');
    divArticle.addClass(article.className);

    var inputArticle = $('<input>');
    inputArticle.addClass(article.note.className);
    inputArticle.attr('type', article.note.type);
    inputArticle.attr('value', article.note.value);

    var divContainer = $('<div>');
    var container = article.container;
    divContainer.addClass(container.className)

    //#region heading
    var divHeading = $('<div>');
    divHeading.addClass(article.container.heading.className);
    var divTitle = $('<div>');
    divTitle.addClass(article.container.heading.title.className);
    var title = $('<b>');
    title.text(article.container.heading.title.text);
    var divCategory = $('<div>');
    divCategory.addClass(article.container.heading.category.className);
    var category = $('<div>');
    category.addClass(article.container.heading.category.className);
    var categ = $('<h6>');
    categ.addClass(article.container.heading.category.categ.className);
    categ.text(article.container.heading.category.categ.text);

    var divDate = $('<div>');
    divDate.addClass(article.container.heading.date.className);
    var date = $('<h6>');
    date.text(article.container.heading.date.text);
    //#endregion

    //#region conditions
    var divConditions = $('<div>');
    divConditions.addClass(article.container.conditions.className);
    var divCommonPrice = $('<div>');
    divCommonPrice.addClass(article.container.conditions.commonPrice.className);
    var price = $('<i>');
    price.addClass(article.container.conditions.commonPrice.price.className);
    var priceText = $('<span>');
    priceText.text(article.container.conditions.commonPrice.price.text);
    var divDeadline = $('<div>');
    divDeadline.addClass(article.container.conditions.deadline.className);
    var calendar = $('<i>');
    calendar.addClass(article.container.conditions.deadline.calendar.className);
    var textCalendar = $('<span>');
    textCalendar.text(article.container.conditions.deadline.calendar.text);
    //#endregion

    //#region Description
    var divDescription = $('<div>');
    var desc = container.description;
    divDescription.addClass(desc.className);
    var pDescription = $('<p>');
    pDescription.addClass(desc.text.className);
    pDescription.text(desc.text.text);
    //#endregion

    //#region Toolbar
    var divToolbar = $('<div>');
    divToolbar.addClass(article.container.toolbar.className);

    var aElemToolbarOpen = $('<a>');
    aElemToolbarOpen.addClass(article.container.toolbar.referenceOpen.className);
    aElemToolbarOpen.attr('onclick', article.container.toolbar.referenceOpen.link);
    var iElemToolbarOpen = $('<i>');
    iElemToolbarOpen.addClass(article.container.toolbar.referenceOpen.open.className);
    aElemToolbarOpen.append(iElemToolbarOpen);

    var aElemToolbarCancel = $('<a>');
    aElemToolbarCancel.addClass(article.container.toolbar.referenceCancel.className);
    aElemToolbarCancel.attr('onclick', article.container.toolbar.referenceCancel.link);
    var iElemToolbarCancel = $('<i>');
    iElemToolbarCancel.addClass(article.container.toolbar.referenceCancel.cancel.className);
    aElemToolbarCancel.append(iElemToolbarCancel);

    var aElemToolbarPending = $('<a>');
    aElemToolbarPending.addClass(article.container.toolbar.referencePending.className);
    aElemToolbarPending.attr('onclick', article.container.toolbar.referencePending.link);
    var iElemToolbarPending = $('<i>');
    iElemToolbarPending.addClass(article.container.toolbar.referencePending.pending.className);
    aElemToolbarPending.append(iElemToolbarPending);
    var aElemToolbarDelete = $('<a>');
    aElemToolbarDelete.addClass(article.container.toolbar.referenceDelete.className);
    aElemToolbarDelete.attr('onclick', article.container.toolbar.referenceDelete.link);
    var iElemToolbarDelete = $('<i>');
    iElemToolbarDelete.addClass(article.container.toolbar.referenceDelete.delete.className);
    aElemToolbarDelete.append(iElemToolbarDelete);
    var spanElemToolbarStatus = $('<span>');
    spanElemToolbarStatus.addClass(article.container.toolbar.referenceStatus.status.className);

    var textStatus = GetTextStatus(article.container.toolbar.referenceStatus.status.text);
    spanElemToolbarStatus.text(textStatus);


        //#endregion

        //#region GoToCompanyButton
    if (article.container.toolbar.referenceStatus.status.text != RequestStatus.Canceled) {
        var divButton = $('<div>');
        divButton.addClass(article.buttonGoCompany.className);
        var iButtonClick = $('<i>');
        iButtonClick.addClass(article.buttonGoCompany.goToCompanyClick.className);
        iButtonClick.attr('onclick', article.buttonGoCompany.goToCompanyClick.link);
        divButton.append(iButtonClick);
    }
    //#endregion

    //#region appendRegion
    calendar.append(textCalendar);
    divTitle.append(title);
    divCategory.append(category);
    category.append(categ);
    divDate.append(date);
    price.append(priceText);
    divCommonPrice.append(price);
    divDeadline.append(calendar);
    if (article.container.toolbar.referenceStatus.status.text!= RequestStatus.Canceled) divToolbar.append(aElemToolbarOpen);
    if (article.container.toolbar.referenceStatus.status.text != RequestStatus.Archived) {
        divToolbar.append(aElemToolbarCancel);
        divToolbar.append(aElemToolbarPending);
        divToolbar.append(aElemToolbarDelete);
    }
    divToolbar.append(spanElemToolbarStatus);
    divConditions.append(divCommonPrice);
    divConditions.append(divDeadline);

    divHeading.append(divTitle);
    divHeading.append(divCategory);
    divHeading.append(divDate);

    divDescription.append(pDescription);

    divContainer.append(divHeading);
    divContainer.append(divConditions);
    divContainer.append(divDescription);
    divContainer.append(divToolbar);

    divArticle.append(inputArticle);
    divArticle.append(divContainer);
    divArticle.append(divButton);
    //#endregion


    return divArticle;
}

function GetTextStatus(enumStatus)
{
    var textStatus;
    switch (enumStatus) {
        case (RequestStatus.New):
            textStatus = "New";
            break;
        case (RequestStatus.Pending):
            textStatus = "Pending";
            break;
        case(RequestStatus.Accepted):
            textStatus = "Accepted";
            break;
        case (RequestStatus.Canceled):
            textStatus = "Canceled";
            break;
        case (RequestStatus.Archived):
            textStatus = "Archived";
            break;
    }
    return textStatus;
}

