﻿var checkCompanyList = 0;

function getCompanyListById(companyId) {
    $("#tabs-company").detach();
    $("#tabsOrderHistory").remove();
    $("#loading").show(1000);
    var token = sessionStorage.getItem('tokenKey');
    
    $.get({
        url: "/API/CompanyAPI/GetOrders",
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        contentType: "application/x-www-form-url; charset=urf-8",
        dataType: "json",
        
        success: function (result) {
            //$.ajaxSettings.headers["Authorization", null];
            var rowAdd = $('#addRow');
            var companyHead = createFormCompanyDomElement();
            rowAdd.append(initFormCompanyDomElement(companyHead.formHead));

            var requestList = $('#comList');
            var headOfCompany =
                '<div class="container">' +
                '<div class="orders-heading">' +
                '<h5><b>Customers list</b></h5>' +
                '</div>' +
                '</div>';
            requestList.append(headOfCompany);
            var countLenght = $("h5#TextMyRequest");
            countLenght.empty();
            countLenght.append("My customers <span class='badge badge-light' id='circle'>" + result.length + "</span>");
            $(result).each(function (i, item) {
                
                var rootCompanyElement = createCompanyDomElement(item);
                requestList.append(initCompanyDomElement(rootCompanyElement.label));
                $("#loading").hide();
            });
            if (checkCompanyList == companyId) {
                $("#tabs-company").detach();
                checkCompanyList = 0;
                chek = 0;
            }
            else {
                checkCompanyList = companyId;
            }
        },
        error: function (error) { console.log(error);}
    });

    function createFormCompanyDomElement() {
        return {
            formHead: {
                className: 'col requests-container',
                id: 'tabs-company',
                sens: {
                    className: 'items',
                    id: 'requestList',
                    container: {
                        className: 'container',
                        id: 'comList',
                    }
                }
            }
        }
    }

    function initFormCompanyDomElement(formHead) {
        var divForm = $('<div>');
        divForm.addClass(formHead.className);
        divForm.attr('id', formHead.id);

        var divItems = $('<div>');
        divItems.addClass(formHead.sens.className);
        divItems.attr('id', formHead.sens.id);
        divForm.append(divItems);

        var divCont = $('<div>');
        divCont.addClass(formHead.sens.container.className);
        divCont.attr('id', formHead.sens.container.id);
        divItems.append(divCont);


        return divForm;
    }


    function createCompanyDomElement(item) {
        return {
            label: {
                className: 'retew',
                container: {
                    className: 'container',
                    item: {
                        className: 'item' + ' ' + item.Order.Id,                       
                        article: {
                            className: 'item-info container',
                            info: {
                                className: 'item-rating',
                                text: 'Request Information',
                            },
                            heading: {
                                className: 'item-info-heading',
                                title: {
                                    className: 'item-title',
                                    text:'Price: '+
                                        item.Request.MinPrice + ' - ' + item.Request.MaxPrice +  '$/'+
                                        'Description: ' + item.Request.Description + '/' +
                                        'Customer name:  ' + 
                                        item.Request.Customer.FirstName +
                                        '  ' + item.Request.Customer.LastName,
                                },
                                date: {
                                    className: 'item-date ml-auto',
                                    textDate: {
                                        className: 'text-muted',
                                        exdate: new Date(item.Request.Deadline).toUTCString().replace('GMT', ''),
                                    },
                                },
                            },
                            rating: {
                                className: 'item-rating',
                                //rate: item.Request.Description,
                            },
                            description: {
                                className: 'item-rating',
                                text: 'Order Information',
                            },
                            conditions: {
                                className: 'item-conditions',
                                price: {
                                    className: 'item-price',
                                    money: {
                                        className: 'fas fa-money-bill',
                                        text: item.Order.Price,
                                    }
                                },
                                deadline: {
                                    className: 'order-deadline',
                                    calendar: {
                                        className: 'far fa-calendar-alt',
                                        text: new Date(item.Request.Date).toUTCString().replace('GMT', ''),
                                    }
                                }
                            },
                        },

                        status: {
                            className: 'badge status-badge float-right ',
                            text: item.Request.Status,
                        },
                        buttonGoToHistory: {
                            className: 'menu-right-button',
                            goToCompanyClick: {
                                className: 'fas fa-angle-right createChat',
                                id: 'btnMessages' + item.Order.Id,
                                dataOrderId: item.Order.Id,
                                dataRequestId: item.Order.RequestId,
                                link: 'getOrderHistories(' + 'btnMessages' + item.Order.Id + ')',
                                // 'getCompanyListById(' + item.Id + ')',
                            }
                        }
                        //'<i class="fas fa-angle-right" id="btnMessages' + item.Id + '" data-order-id="' + item.Id + '" data-request-id="' + requestId + '" onclick="getOrderHistories(' + 'btnMessages' + item.Id + ')"></i>' 
                    }
                }
            }
        }
    }


    function initCompanyDomElement(label) {
        var divLabel = $('<div>');
        divLabel.addClass(label.className);
        var divContainer = $('<div>');
        divContainer.addClass(label.container.className);
        divLabel.append(divContainer);

        var divItem = $('<div>');
        divItem.addClass(label.container.item.className);
        divItem.text(label.container.item.text);
        divContainer.append(divItem);

        var divArticle = $('<div>');
        divArticle.addClass(label.container.item.article.className);
        divItem.append(divArticle);

        var divInfo = $('<div>');
        divInfo.addClass(label.container.item.article.info);
        var pElemInfo = $('<b>');
        pElemInfo.text(label.container.item.article.info.text);
        divArticle.append(pElemInfo);
        divArticle.append(divInfo);

        var divHeading = $('<div>');
        divHeading.addClass(label.container.item.article.heading.className);
        var divTitle = $('<p>');
        divTitle.addClass(label.container.item.article.heading.title.className);
        divTitle.text(label.container.item.article.heading.title.text);
        divHeading.append(divTitle);
        divArticle.append(divHeading);

        var divDate = $('<div>');
        divDate.addClass(label.container.item.article.heading.date.className);
        var divTextDate = $('<div>');
        divTextDate.addClass(label.container.item.article.heading.date.textDate.className);
        divTextDate.text(label.container.item.article.heading.date.textDate.exdate);
        divDate.append(divTextDate);
        divHeading.append(divDate);

        //var divRating = $('<div>');
        //divRating.addClass(label.container.item.article.rating.className);
        //var spanRate = $('<p>');
        //spanRate.text(label.container.item.article.rating.rate);
        //divRating.append(spanRate);
        //divArticle.append(divRating);

        var divDescription = $('<div>');
        divDescription.addClass(label.container.item.article.description);
        var pElemDesc = $('<b>');
        pElemDesc.text(label.container.item.article.description.text);
        divArticle.append(pElemDesc);
        divArticle.append(divDescription);

        var divCondition = $('<div>');
        divCondition.addClass(label.container.item.article.conditions.className);
        var divPrice = $('<div>');
        divPrice.addClass(label.container.item.article.conditions.price.className);
        var iElemMoney = $('<i>');
        iElemMoney.addClass(label.container.item.article.conditions.price.money.className);
        var spanElemMoney = $('<span>');
        spanElemMoney.text(label.container.item.article.conditions.price.money.text);
        divPrice.append(iElemMoney);
        divPrice.append(spanElemMoney);
        divCondition.append(divPrice);
        divArticle.append(divCondition);

        var divDeadLine = $('<div>');
        divDeadLine.addClass(label.container.item.article.conditions.deadline.className);
        var iElemCalendar = $('<i>');
        iElemCalendar.addClass(label.container.item.article.conditions.deadline.calendar.className);
        var spanElemCalendar = $('<span>');
        spanElemCalendar.text(label.container.item.article.conditions.deadline.calendar.text);
        divDeadLine.append(iElemCalendar);
        divDeadLine.append(spanElemCalendar);
        divCondition.append(divDeadLine);

        var spanElemStatus = $('<span>');
        spanElemStatus.addClass(label.container.item.status.className);
        var textStatus = GetTextStatus(label.container.item.status.text);
        spanElemStatus.text(textStatus);
        divDescription.append(spanElemStatus);

        var divGotoHistory = $('<div>');
        divGotoHistory.addClass(label.container.item.buttonGoToHistory.className);
        var iElemButton = $('<i>');
        iElemButton.addClass(label.container.item.buttonGoToHistory.goToCompanyClick.className);
        iElemButton.attr('id', label.container.item.buttonGoToHistory.goToCompanyClick.id);
        iElemButton.attr('data-order-id', label.container.item.buttonGoToHistory.goToCompanyClick.dataOrderId);
        iElemButton.attr('data-request-id', label.container.item.buttonGoToHistory.goToCompanyClick.dataRequestId);
        iElemButton.attr('onclick', label.container.item.buttonGoToHistory.goToCompanyClick.link);
        divGotoHistory.append(iElemButton);
        divItem.append(divGotoHistory);

        return divLabel;
    }
}


$(document).ready(function () {
    getCompanyListById(getCookie("UserId"));
});
function GetTextStatus(enumStatus) {
    var textStatus;
    switch (enumStatus) {
        case (RequestStatus.New):
            textStatus = "New";
            break;
        case (RequestStatus.Pending):
            textStatus = "Pending";
            break;
        case (RequestStatus.Accepted):
            textStatus = "Accepted";
            break;
        case (RequestStatus.Canceled):
            textStatus = "Canceled";
            break;
        case (RequestStatus.Archived):
            textStatus = "Archived";
            break;
    }
    return textStatus;
}