﻿using BussinessLogicLayer.Core;
using BussinessLogicLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Repositories
{
    public interface ICustomerRepository : IRepository<Customer, string>
    {
        List<string> GetValidCustomerIds(List<string> customerIds);
        IEnumerable<Customer> GetCustomersFromOrderHistories(IEnumerable<string> CustomerIds);


    }
}
