﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessLogicLayer.Core;
using BussinessLogicLayer.Models;

namespace BussinessLogicLayer.Repositories
{
    public interface IOrderRepository : IRepository<Order, int>
    {
        List<Order> GetByRequestId(int id);
        List<Request> GetByCompanyId(string id);
        Order GetByIdWithOrderHistory(int id);
        List<Order> Find(int requestId);
        List<Order> GetAllWithOrderHistory();
        List<OrderHistory> GetLastOrderHistoryByOrder(int idRequest);
        List<Order> GetOrdersByUserId(string CurrentUserId);

    }
}
