﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Models.DTO
{
    public class CurrentChatOrderHistoryData
    {
        public int Id { get; set; }
        public DateTime ScheduleDate { get; set; }

        public string Price { get; set; }

        public string UserType { get; set; }

        public static CurrentChatOrderHistoryData Init(OrderHistory orderHistory, InformationAboutUser informationAboutUser)
        {
            return new CurrentChatOrderHistoryData()
            {
                Id = orderHistory.Id,
                Price = orderHistory.Price,
                ScheduleDate = orderHistory.ScheduleDate,
                UserType = informationAboutUser.TypeOfUser
            };
        }
    }
}
