﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Models.DTO
{
    public class CustomerNotificationsDTO
    {
        public int Id { get; set; }

        public string Price { get; set; }

        public DateTime ScheduleDate { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

        public DateTime CreateDate { get; set; }

        public string Message { get; set; }

        public bool IsShown { get; set; }

        public int OrderId { get; set; }

        public int RequestId { get; set; }

        public static CustomerNotificationsDTO Init(OrderHistory orderHistory, string name, string message)
        {
            return new CustomerNotificationsDTO
            {
                Price = orderHistory.Price,
                ScheduleDate = orderHistory.ScheduleDate,
                CreateDate = orderHistory.UpdateDate,
                //Status = orderHistory.Status,
                Name = name,
                Message = message,
                IsShown = orderHistory.IsShown,
                Id = orderHistory.Id,
                OrderId = orderHistory.Order.Id,
                RequestId = orderHistory.Order.RequestId

            };
        }
    }
}
