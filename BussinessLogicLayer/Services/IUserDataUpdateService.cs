﻿using BussinessLogicLayer.Models.DTO;

namespace BussinessLogicLayer.Services
{
    public interface IUserDataUpdateService
    {
        CustomerUpdateProfileDataDTO GetCustomerProfileData(string currentUserId);
        void UpdateCustomerProfileData(CustomerUpdateProfileDataDTO updateModel);
        CompanyUpdateProfileDataDTO GetCompanyProfileData(string currentUserId);
        void UpdateCompanyProfileData(CompanyUpdateProfileDataDTO updateModel);

    }
}
