﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BussinessLogicLayer.Models
{
    public class Offer
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int CategoryId { get; set; }

        public Category Category { get; set; }

        public ICollection<CompanyOffer> CompanyOffers { get; set; }
        public ICollection<Request> Requests { get; set; }
    }
}