﻿$("#companyNameTextBox").change(function () {
    validateCompanyNameTextBox();
});
$("#companyNameTextBox").focusout(function () {
    validateCompanyNameTextBox();
});

$("#companyAddressTextBox").change(function () {
    validateCompanyAddressTextBox();
});
$("#companyAddressTextBox").focusout(function () {
    validateCompanyAddressTextBox();
});

$("#companyPhoneNumberTextBox").change(function () {
    validateCompanyPhoneNumberTextBox();
});
$("#companyPhoneNumberTextBox").focusout(function () {
    validateCompanyPhoneNumberTextBox();
});

function CheckIfAllDigits(value) {
    return /^\d+$/.test(value);
}

function resetCompnanyProfileUpdateValidationStyles() {
    $("#companyNameTextBox").removeClass('errorInput');
    $("#companyAddressTextBox").removeClass('errorInput');
    $("#companyPhoneNumberTextBox").removeClass('errorInput');

    $("#invalidCompanyNameOrAddress").hide();
    $("#invalidCompanyPhoneNumber").hide();
}

function validateCompanyNameTextBox() {
    let textBox = $("#companyNameTextBox");
    if (textBox.val() === null || textBox.val().trim() === '') {
        textBox.addClass('errorInput');
        return false;
    }
    else {
        textBox.removeClass('errorInput');
        return true;
    }
}

function validateCompanyAddressTextBox() {
    let textBox = $("#companyAddressTextBox");
    if (textBox.val() === null || textBox.val().trim() === '') {
        textBox.addClass('errorInput');
        return false;
    }
    else {
        textBox.removeClass('errorInput');
        return true;
    }
}

function validateCompanyPhoneNumberTextBox() {
    let phoneNumberTextBox = $("#companyPhoneNumberTextBox");

    if (CheckIfAllDigits(phoneNumberTextBox.val()) && Number.parseInt(phoneNumberTextBox.val()) > 0) {
        phoneNumberTextBox.removeClass('errorInput');
        return true;
    }
    else {
        phoneNumberTextBox.addClass('errorInput');
        return false;
    }
}