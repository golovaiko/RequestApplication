﻿using BussinessLogicLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BussinessLogicLayer.Models;
using System.Threading.Tasks;
using BussinessLogicLayer.Services.Statuses;

namespace BussinessLogicLayer.Services
{
    public class RequestStatusCountService : IRequestStatusCountService
    {
        IRequestRepository _requestRepository;

        public RequestStatusCountService()
        {

        }
        public RequestStatusCountService(IRequestRepository requestRepository)
        {
            _requestRepository = requestRepository;
        }
        public Dictionary<string, int> GetRequestsStatusCount(string UserId)
        {
            var CurrentUserRequests = _requestRepository.GetRequestsStatusByUserId(UserId);
            Dictionary<string, int> RequestsStatusCount = new Dictionary<string, int>();
            int AllRequests = CurrentUserRequests.Count();
            int NewStatus = 0;
            int ArchivedStatus = 0;
            int PendingStatus = 0;
            int CanceledStatus = 0;
            int AcceptedStatus = 0;
            foreach (var status in CurrentUserRequests)
            {
                switch (status)
                {
                    case (int)StatusesForRequest.New :
                        NewStatus++;
                        break;
                    case (int)StatusesForRequest.Archived:
                        ArchivedStatus++;
                        break;
                    case (int)StatusesForRequest.Pending:
                        PendingStatus++;
                        break;
                    case (int)StatusesForRequest.Canceled:
                        CanceledStatus++;
                        break;
                    case (int)StatusesForRequest.Accepted:
                        AcceptedStatus++;
                        break;

                }
            }
            RequestsStatusCount.Add("All", AllRequests);
            RequestsStatusCount.Add("New", NewStatus);
            RequestsStatusCount.Add("Archived", ArchivedStatus);
            RequestsStatusCount.Add("Canceled", CanceledStatus);
            RequestsStatusCount.Add("Accepted", AcceptedStatus);
            RequestsStatusCount.Add("Pending", PendingStatus);
            return RequestsStatusCount;
        }




    }
}
