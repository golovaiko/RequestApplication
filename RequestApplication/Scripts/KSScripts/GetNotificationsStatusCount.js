﻿var stopInterval;
var token = sessionStorage.getItem('tokenKey');

$(document).ready(function () {
    GetActiveNotifications();
    GetRequestsStatusCount();
    setInterval(GetRequestsStatusCount, 5000);
    setInterval(GetActiveNotifications, 5000);
    stopInterval = setInterval(GetNotifications, 5000);
});

$(document).on('click', "#NotificationLink", function () {
    clearInterval(stopInterval);
});
$(document).on('click', "#buttonClose", function () {
    stopInterval = setInterval(GetNotifications, 5000);
    
});


function GetActiveNotifications() {
    $.ajax({
        url: '/api/NotificationsAPI/GetCustomerNewNotificationsCount',
        type: 'GET',
        dataType: 'json',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (count) {
            $('#notificationBadge').text(count);
        },
        error: function () {

        }
    });
}

function GetRequestsStatusCount() {
    $.ajax({
        url: '/api/RequestAPI/GetRequestsStatusCount',
        type: 'GET',
        dataType: 'json',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (StatusCount) {
            $("#allRequests").children('span').text(StatusCount.All);
            $("#StatusNew").children('span').text(StatusCount.New);
            $("#StatusPending").children('span').text(StatusCount.Pending);
            $("#StatusAccepted").children('span').text(StatusCount.Accepted);
            $("#StatusCanceled").children('span').text(StatusCount.Canceled);
            $("#StatusArchived").children('span').text(StatusCount.Archived);
        },
        error: function () {
           // alert('Error!');
        }
    });
};

