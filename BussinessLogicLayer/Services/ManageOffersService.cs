﻿using BussinessLogicLayer.Models;
using BussinessLogicLayer.Models.DTO;
using BussinessLogicLayer.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace BussinessLogicLayer.Services
{
    public class ManageOffersService : IManageOffersService
    {
        readonly IOfferRepository _offerRepository;
        readonly ICompanyOfferRepository _companyOfferRepository;

        public ManageOffersService(
            IOfferRepository offerRepository,
            ICompanyOfferRepository companyOfferRepository)
        {
            _offerRepository = offerRepository;
            _companyOfferRepository = companyOfferRepository;
         }

        public OfferCollectionDTO GetProvidedOffers(string companyId, int categoryId)
        {
            var offers = _offerRepository.GetAll().Where(a => a.CategoryId == categoryId).ToList();
            List<OfferDTO> result = new List<OfferDTO>();
            foreach (var item in offers)
            {
                var currentCompanyOffer = _companyOfferRepository.GetAll().Where(x => x.CompanyId == companyId && x.OfferId == item.Id).LastOrDefault();
                if (currentCompanyOffer == null || currentCompanyOffer.OfferId != item.Id)
                {
                    result.Add(new OfferDTO() { Offer = item, IsProviding = _companyOfferRepository.IsProviding(item.Id, companyId) });
                }
                else
                {
                    result.Add(new OfferDTO() { Offer = item, IsProviding = _companyOfferRepository.IsProviding(item.Id, companyId), OfferPrice = currentCompanyOffer.OfferPrice });
                }
            }
            return new OfferCollectionDTO() { OfferDTOs = result };
        }

        public void SaveProvidedOffers(OfferCollectionDTO model)
        {
            foreach (var item in model.OfferDTOs)
            {
                if (item.IsProviding)
                {
                    if (!_companyOfferRepository.IsProviding(item.Offer.Id, model.CompanyId))
                    {
                        _companyOfferRepository.Create(new CompanyOffer() { OfferId = item.Offer.Id, CompanyId = model.CompanyId, OfferPrice = item.OfferPrice.ToString() });
                    }
                }
                else
                {
                    CompanyOffer id;
                    if (_companyOfferRepository.IsProviding(item.Offer.Id, model.CompanyId, out id))
                    {
                        _companyOfferRepository.Delete(id);
                    }
                }
            }
            _companyOfferRepository.SaveChanges();
        }
    }
}
