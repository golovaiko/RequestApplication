﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BussinessLogicLayer.Models;
using BussinessLogicLayer.Repositories;
using Ninject;

namespace RequestApplication.API
{
    public class CategoryController : ApiController
    {
        private ICategoryRepository _categoryRepository;

        [Inject]
        public CategoryController(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }        
        public CategoryController() { }
        [HttpGet]
        public List<Category> GC()
        {
            return _categoryRepository.GetAll().ToList();
        } 
    }
}
