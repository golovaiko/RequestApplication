﻿using BussinessLogicLayer.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace RequestApplication.Models
{
    public class OrderHistoryVM
    {
        public int OrderHistoryId { get; set; }

        public int Status { get; set; }

        public string Price { get; set; }
        public DateTime ScheduleDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public Order Order { get; set; }

        public string UserId { get; set; }      

        public string TypeOfUser { get; set; }

        public string NameUser { get; set; }

        public string Changed { get; set; }

        public static OrderHistoryVM Init(OrderHistory orderHistory, InformationAboutUser informationAboutUser)
        {
            return new OrderHistoryVM()
            {
                OrderHistoryId = orderHistory.Id,
                Status = orderHistory.Status,
                Price = orderHistory.Price,
                //ScheduleDate = orderHistory.ScheduleDate.ToString("f", CultureInfo.CreateSpecificCulture("en-US")),
                //UpdateDate = orderHistory.UpdateDate.ToString("f", CultureInfo.CreateSpecificCulture("en-US")),
                ScheduleDate = orderHistory.ScheduleDate,
                UpdateDate = orderHistory.UpdateDate,
                Order = orderHistory.Order,
                UserId = orderHistory.UserId,
                Changed = orderHistory.Changed,
                TypeOfUser = informationAboutUser.TypeOfUser,
                NameUser = informationAboutUser.NameOfUser,
            };

        }
    }
}