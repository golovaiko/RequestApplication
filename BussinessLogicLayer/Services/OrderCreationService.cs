﻿using BussinessLogicLayer.Models;
using BussinessLogicLayer.Repositories;
using System;
using System.Linq;

namespace BussinessLogicLayer.Services
{
    public class OrderCreationService : IOrderCreationService
    {
        readonly IRequestRepository _requestRepository;
        readonly IOrderRepository _orderRepository;
        readonly ICompanyRepository _companyRepository;
        readonly IOfferRepository _offerRepository;
        readonly ICategoryRepository _categoryRepository;
        readonly IOrderHistoryRepository _orderHistoryRepository;
        readonly ICompanyOfferRepository _companyOfferRepository;

        public OrderCreationService(IRequestRepository requestRepository, IOrderRepository orderRepository, ICompanyRepository companyRepository,
            IOfferRepository offerRepository, ICategoryRepository categoryRepository, IOrderHistoryRepository orderHistoryRepository,
            ICompanyOfferRepository companyOfferRepository)
        {
            _requestRepository = requestRepository;
            _orderRepository = orderRepository;
            _companyRepository = companyRepository;
            _offerRepository = offerRepository;
            _categoryRepository = categoryRepository;
            _orderHistoryRepository = orderHistoryRepository;
            _companyOfferRepository = companyOfferRepository;
        }

        public void CreateOrdersByRequest(DateTime deadline)
        {
            var justMadeRequest = _requestRepository.GetLastRequest();
            var ListCompanies = _companyRepository.GetCompaniesByOfferId(justMadeRequest.OfferId);

            foreach (var i in ListCompanies)
            {
                var compOffer = _companyOfferRepository.GetNeededCompanyOffer(i, justMadeRequest);
                if (int.Parse(compOffer.OfferPrice) >= justMadeRequest.MinPrice && int.Parse(compOffer.OfferPrice) <= justMadeRequest.MaxPrice)
                {
                    Order order = new Order()
                    {
                        CompanyId = i.Id,
                        ExecutionDate = deadline,
                        RequestId = justMadeRequest.Id,
                        Price = compOffer.OfferPrice
                    };

                    _orderRepository.Create(order);
                    _orderRepository.SaveChanges();
                }
            }
        }
    }
}
