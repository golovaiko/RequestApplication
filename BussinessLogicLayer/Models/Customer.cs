﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Models
{
    public class Customer
    {
        public string Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Gender { get; set; }
        [Required]
        public DateTime BirthDate { get; set; }

        public byte[] ProfileImage { get; set; }

        public string PhoneNumber { get; set; }

        public ICollection<Request> Requests { get; set; }
    }
}