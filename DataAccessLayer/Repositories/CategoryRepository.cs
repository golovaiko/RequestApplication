﻿using BussinessLogicLayer.Models;
using BussinessLogicLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class CategoryRepository : Repository<Category , int>, ICategoryRepository
    {
        public CategoryRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
    }
}
