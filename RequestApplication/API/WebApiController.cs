﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace RequestApplication.API
{
    [Authorize]
    public class WebApiController : ApiController
    {

        public IHttpActionResult Get()
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;

            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });

            return Ok(claims);
        }   

        // GET api/WebApi/5  
        public string Get(int id)
        {
            return "Hello Authorized API with ID = " + id;
        }

        // POST api/WebApi  
        public void Post([FromBody]string value)
        {
        }

        // PUT api/WebApi/5  
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/WebApi/5  
        public void Delete(int id)
        {
        }

    }
}