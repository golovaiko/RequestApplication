﻿using BussinessLogicLayer.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using BussinessLogicLayer.Models;

namespace DataAccessLayer.Repositories
{
    public class Repository<TEntity, TId> : IRepository<TEntity , TId> where TEntity : class
    {
        protected readonly UnitOfWork UnitOfWork;

        public Repository(UnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public TEntity GetById(TId id)
        {
            return UnitOfWork.Context.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return UnitOfWork.Context.Set<TEntity>().ToList();
        }

        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> filter = null)
        {
            IEnumerable<TEntity> items;
            if (filter == null)
            {
                items = UnitOfWork.Context.Set<TEntity>().ToList();
            }
            else
            {
                items = UnitOfWork.Context.Set<TEntity>().Where(filter).ToList();
            }
            return items;
        }

        public void Create(TEntity entity)
        {
            UnitOfWork.Context.Set<TEntity>().Attach(entity);
            UnitOfWork.Context.Set<TEntity>().Add(entity);
        }

        public void Delete(TEntity entity)
        {
            UnitOfWork.Context.Set<TEntity>().Attach(entity);
            UnitOfWork.Context.Set<TEntity>().Remove(entity);
        }

        public void Update(TEntity entity)
        {
            UnitOfWork.Context.Set<TEntity>().Attach(entity);
            UnitOfWork.Context.Entry(entity).State = EntityState.Modified;            
        }

        public void SaveChanges()
        {
            UnitOfWork.SaveChanges();
        }

        public void Dispose()
        {
            UnitOfWork.Dispose();
        }
    }
}
