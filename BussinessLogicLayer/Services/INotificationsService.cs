﻿using BussinessLogicLayer.Models;
using BussinessLogicLayer.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Services
{
    public interface INotificationsService
    {
        List<OrderHistory> GetCustomerOrderHistories(string CurrentUser);
        IEnumerable<Company> GetCompaniesFromOrderHistories(List<string> UserIds);
        List<CustomerNotificationsDTO> GetCustomersNotifications(string userId);
        List<OrderHistory> GetCompanyOrderHistories(string currentUserId);
        void ChangeNotificationStatus(int id);
        List<CompanyNotificationsDTO> GetCompanysNotifications(string id);
        void DeleteNotificationsByOrderId(int orderId);
        CurrentChatOrderHistoryData GetCurrentChatOrderHistory(int OrderId);



    }
}
