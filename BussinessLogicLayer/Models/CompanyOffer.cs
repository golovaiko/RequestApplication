﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Models
{
    public class CompanyOffer
    {
        [Key]
        [Column(Order = 1)]
        public int OfferId { get; set; }
        [Key]
        [Column(Order = 2)]
        public string CompanyId { get; set; }

        public string OfferPrice { get; set; }

        public Company Company { get; set; }
        public Offer Offer { get; set; }
    }
}
