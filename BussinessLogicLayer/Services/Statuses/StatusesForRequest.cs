﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Services.Statuses
{
    public enum StatusesForRequest
    {
        New = 1,
        Pending,
        Accepted,
        Canceled,
        Archived,
        All
    }
}
