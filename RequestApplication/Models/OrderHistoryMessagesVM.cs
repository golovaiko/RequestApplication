﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RequestApplication.Models
{
    public class OrderHistoryMessagesVM
    {
        public int OrderId { get; set; }
        public int RequestId { get; set; }
    }
}