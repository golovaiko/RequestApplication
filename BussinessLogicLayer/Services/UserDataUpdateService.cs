﻿using BussinessLogicLayer.Models.DTO;
using BussinessLogicLayer.Repositories;
using System;
using System.Globalization;

namespace BussinessLogicLayer.Services
{
    public class UserDataUpdateService : IUserDataUpdateService
    {
        readonly ICompanyRepository _companyRepository;
        readonly ICustomerRepository _customerRepository;

        public UserDataUpdateService(ICompanyRepository companyRepository, ICustomerRepository customerRepository)
        {
            _companyRepository = companyRepository;
            _customerRepository = customerRepository;
        }

        public CustomerUpdateProfileDataDTO GetCustomerProfileData(string currentUserId)
        {
            var currentUser = _customerRepository.GetById(currentUserId);
            CustomerUpdateProfileDataDTO dataToUpdate = new CustomerUpdateProfileDataDTO
            {
                Name = currentUser.FirstName,
                Surname = currentUser.LastName,
                BirthDate = currentUser.BirthDate.ToString("yyyy-MM-dd"),
                Gender = currentUser.Gender,
                PhoneNumber = currentUser.PhoneNumber,
                ProfileImage = currentUser.ProfileImage
            };
            return dataToUpdate;
        }
        public void UpdateCustomerProfileData(CustomerUpdateProfileDataDTO updateModel)
        {
            var currentCustomer = _customerRepository.GetById(updateModel.UserId);
            DateTime updatedBirthDate = DateTime.ParseExact(updateModel.BirthDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);

            currentCustomer.FirstName = updateModel.Name;
            currentCustomer.LastName = updateModel.Surname;
            currentCustomer.Gender = updateModel.Gender;
            currentCustomer.BirthDate = updatedBirthDate;
            currentCustomer.PhoneNumber = updateModel.PhoneNumber;
            if (updateModel.ProfileImage != null && currentCustomer.Requests != null)
            {
                currentCustomer.ProfileImage = updateModel.ProfileImage;
            }


            _customerRepository.Update(currentCustomer);
            _customerRepository.SaveChanges();
        }

        public CompanyUpdateProfileDataDTO GetCompanyProfileData(string currentUserId)
        {
            var currentUser = _companyRepository.GetById(currentUserId);
            CompanyUpdateProfileDataDTO dataToUpdate = new CompanyUpdateProfileDataDTO
            {
                Name = currentUser.Name,
                Address = currentUser.Address,
                PhoneNumber = currentUser.PhoneNumber
            };
            return dataToUpdate;
        }

        public void UpdateCompanyProfileData(CompanyUpdateProfileDataDTO updateModel)
        {
            var currentCompany = _companyRepository.GetById(updateModel.UserId);

            currentCompany.Name = updateModel.Name;
            currentCompany.Address = updateModel.Address;
            currentCompany.PhoneNumber = updateModel.PhoneNumber;

            _companyRepository.Update(currentCompany);
            _companyRepository.SaveChanges();
        }
    }
}
