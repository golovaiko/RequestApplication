﻿var token = sessionStorage.getItem('tokenKey');

$("#updateProfileDataButton").click(function () {
    GetCustomerDataAjax();
    resetCustomerProfileUpdateValidationStyles();
})

$("#saveProfileChangesButton").click(function () {
    UpdateCustomerDataAjax();
})

GetCustomerDataAjax();

function GetCustomerDataAjax() {
    $.ajax({
        url: '/API/UserDataUpdateAPI/GetCustomerProfileData',
        type: 'GET',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (user) {
            $("#nameTextBox").val(user.Name),
                $("#surnameTextBox").val(user.Surname),
                $("#birthdayTextBox").val(user.BirthDate),
                $("#genderTextBox").val(user.Gender),
                $("#phoneNumberTextBox").val(user.PhoneNumber),
                $("#customerNameLabel").text(user.Name + ' ' + user.Surname)
        },
        error: function (error) { console.log(error) }
    });
}

function UpdateCustomerDataAjax() {
    if (Number.parseInt($("#invalidPhoneNumber").val())) {
        $("#invalidPhoneNumber").show();
        return;
    }

    if (!validateNameTextBox() || !validateSurnameTextBox()) {
        $("#invalidNameOrSurname").show();
        return;
    }

    $("#invalidNameOrSurname").hide();
    $("#invalidPhoneNumber").hide();

    $.ajax({
        url: '/API/UserDataUpdateAPI/UpdateCustomerProfileData',
        type: "POST",
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
        data: {
            Name: $("#nameTextBox").val().trim(),
            Surname: $("#surnameTextBox").val(),
            BirthDate: $("#birthdayTextBox").val(),
            Gender: $("#genderTextBox").val(),
            PhoneNumber: $("#phoneNumberTextBox").val(),
            UserId: ''
        },
        dataType: "json",
        success: function () {
            $.ambiance({
                message: "Saved successfull!",
                title: "Success!",
                type: "success"
            })
            $("#closeCustomerDataUpdateModal").click();
        },
        error: function () {
            $.ambiance({
                message: "Error try again please!",
                title: "Error!",
                type: "error"
            })
            $("#closeCustomerDataUpdateModal").click();
        }
    });

}