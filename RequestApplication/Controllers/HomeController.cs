﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using BussinessLogicLayer.Models;
using BussinessLogicLayer.Repositories;
using Ninject;
using RequestApplication.Models;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RequestApplication.Controllers
{
    public class HomeController : Controller
    {
        readonly ICustomerRepository _customerRepository;
        readonly ICompanyRepository _companyRepository;

        private ApplicationUserManager _userManager;

        public HomeController()
        {

        }

        [Inject]
        public HomeController(ICustomerRepository customerRepository, ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
            _customerRepository = customerRepository;
        }

        public HomeController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                string roles = UserManager.GetRoles(User.Identity.GetUserId()).First();
                if (roles == "customer")
                {
                    return RedirectToAction("HomeCustomerView", "Home");
                }
                else if (roles == "company")
                {
                    return RedirectToAction("HomePageCompany", "Company");
                }
            }
            
            return View();
        }

        [Authorize(Roles = "customer")]
        public ActionResult HomeCustomerView()
        {
            
            return View();

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();

        }

        public ActionResult HeadFoot()
        {
            return View();
        }

        [Authorize(Roles = "customer")]
        public FileContentResult GetCustomerPhoto()
        {
            var userImage = _customerRepository.GetById(User.Identity.GetUserId());
            return new FileContentResult(userImage.ProfileImage, "image/jpeg");
        }

        [Authorize(Roles = "company")]
        public FileContentResult GetCompanyPhoto()
        {
            var userImage = _companyRepository.GetById(User.Identity.GetUserId());
            return new FileContentResult(userImage.ProfileImage, "image/jpeg");
        }
    }
}