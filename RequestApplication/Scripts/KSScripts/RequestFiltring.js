﻿$(document).on("click", 'div.filterStatus', function () {
    $('div#tabs-company').remove();
    $('div#tabsOrderHistory').remove();
    $('.item').remove();
    var CurrentStatus = $(this).children('b:first').text().trim();
    switch (CurrentStatus) {
        case 'All':
            GetRequestsForCurrentUserByStatus(RequestStatus.All);
            break;
        case 'New':
            GetRequestsForCurrentUserByStatus(RequestStatus.New);
            break;
        case 'Pending':
            GetRequestsForCurrentUserByStatus(RequestStatus.Pending);
            break;
        case 'Accepted':
            GetRequestsForCurrentUserByStatus(RequestStatus.Accepted);
            break;
        case 'Canceled':
            GetRequestsForCurrentUserByStatus(RequestStatus.Canceled);
            break;
        case 'Archived':
            GetRequestsForCurrentUserByStatus(RequestStatus.Archived);
            break;
    }
    $('.StatusCountValue').show();
});



