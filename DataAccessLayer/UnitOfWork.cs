﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessLogicLayer.Core;
using DataAccessLayer.Context;

namespace DataAccessLayer
{
    public class UnitOfWork : IUnitOfWork
    {
        public RequestDBContext Context { get; set; }

        public UnitOfWork(RequestDBContext context)
        {
            Context = context;
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
