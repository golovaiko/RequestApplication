﻿using BussinessLogicLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RequestApplication.Models
{
    public class OrderListModel
    {
        public Order Order { get; set; }
        public Request Request { get; set; }
        public Company Company { get; set; }

        public static OrderListModel Init(Request request, Order order, Company company)
        {
            OrderListModel olm = new OrderListModel()
            {
                Request  = new Request(),
                Order = new Order(),
                Company= new Company()
            };
            return olm;
        }

    }


}