﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer.Models.DTO
{
    public class OfferCollectionDTO
    {
        [Required]
        public List<OfferDTO> OfferDTOs { get; set; }
        
        public string CompanyId { get; set; }
    }
}
