﻿using BussinessLogicLayer.Models.DTO;
using BussinessLogicLayer.Services;
using System.Web.Http;
using System.Web.Mvc;

namespace RequestApplication.API
{
    public class UserDataUpdateAPIController : BaseApiController
    {
        readonly IUserDataUpdateService _userDataUpdateService;

        public UserDataUpdateAPIController(IUserDataUpdateService userDataUpdateService)
        {
            _userDataUpdateService = userDataUpdateService;  
        }

        [System.Web.Http.Authorize]
        public CustomerUpdateProfileDataDTO GetCustomerProfileData()
        {
            return _userDataUpdateService.GetCustomerProfileData(UserId);
        }

        [System.Web.Http.Authorize]
        public void UpdateCustomerProfileData(CustomerUpdateProfileDataDTO updateModel)
        {
            updateModel.UserId = UserId;
            if (ModelState.IsValid)
            {
                _userDataUpdateService.UpdateCustomerProfileData(updateModel);
            }
        }

        [System.Web.Http.Authorize]
        public CompanyUpdateProfileDataDTO GetCompanyProfileData()
        {
            return _userDataUpdateService.GetCompanyProfileData(UserId);
        }

        [System.Web.Http.Authorize]
        public void UpdateCompanyProfileData(CompanyUpdateProfileDataDTO updateModel)
        {
            updateModel.UserId = UserId;
            if (ModelState.IsValid)
            {
                _userDataUpdateService.UpdateCompanyProfileData(updateModel);
            }
        }
    }
}