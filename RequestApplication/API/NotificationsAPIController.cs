﻿using BussinessLogicLayer.Models;
using BussinessLogicLayer.Models.DTO;
using BussinessLogicLayer.Repositories;
using BussinessLogicLayer.Services;
using Microsoft.AspNet.Identity;
using Ninject;
using RequestApplication.Controllers;
using RequestApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RequestApplication.API
{
    [Authorize]
    public class NotificationsAPIController : BaseApiController
    {
        readonly INotificationsService _notificationsService;
        readonly IOrderHistoryRepository _orderHistoryRepository;
        readonly IOrderRepository _orderRepository;
        readonly ICustomerRepository _customerRepository;
        readonly IOrderHistoryService _orderHistoryService;

        public NotificationsAPIController()
        {

        }

        public NotificationsAPIController(IOrderRepository orderRepository, ICustomerRepository customerRepository, IOrderHistoryRepository orderHistoryRepository, INotificationsService notificationsService,
            IOrderHistoryService orderHistoryService)
        {
            _customerRepository = customerRepository;
            _notificationsService = notificationsService;
            _orderHistoryRepository = orderHistoryRepository;
            _orderRepository = orderRepository;
            _orderHistoryService = orderHistoryService;

        }

        [HttpGet]
        public List<CustomerNotificationsDTO> GetNotificationsRequests()
        {
            return _notificationsService.GetCustomersNotifications(UserId);
        }

        [HttpGet]
        public List<CompanyNotificationsDTO> GetCompanyNotifications()
        {
            return _notificationsService.GetCompanysNotifications(UserId);
        }


        [HttpPost]
        public void isActive(int id)
        {
            _notificationsService.ChangeNotificationStatus(id);
        }
        

        [HttpGet]
        public CurrentChatOrderHistoryData GetCurrentOrderHistory(int OrderId)
        {
            return _notificationsService.GetCurrentChatOrderHistory(OrderId);
        }

        [HttpPost]
        public void DeleteOpenedChatNotifications(int orderId)
        {
            _notificationsService.DeleteNotificationsByOrderId(orderId);
        }


        [HttpGet]
        public int GetCustomerNewNotificationsCount()
        {
            return _notificationsService.GetCustomerOrderHistories(UserId).Count();
        }

    }
}
